<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/views/admin/app_base.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="format-detection" content="telephone=no" />
    <meta content="email=no" name="format-detection" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <%@ include file="/WEB-INF/views/admin/app_js_css.jsp"%>
    <!--引入 lrz 插件 用于压缩图片-->
    <script type="text/javascript" src="<%=basePath%>/static/admin/app/js/localResizeIMG/lrz.bundle.js"></script>
    <script rel="script" src="<%=basePath%>/static/admin/app/js/fileUpload.js"></script>
    <script rel="script" src="<%=basePath%>/static/admin/app/js/shop_update.js"></script>
    <title>完善信息</title>
</head>
<body>
<div class="viewport">
<header>
	<div class="header-title">
        <a href="<%=basePath%>/admin/shop-info" class="return"><i class="iconfont"></i>返回</a>
        <h2>编辑店铺信息</h2>
    </div>
</header>
<section>
    <form role="form">
        <div class="form-box">
        	<input id="id" type="hidden" class="form-control" readonly value="${shop.id}">
            <div class="form-group">
                <label><em class="red">*</em>店铺编码</label>
                <input id="code" type="text" class="form-control" readonly  placeholder="您的店铺的唯一码" value="${shop.code}">
            </div>
            <div class="form-group">
                <label><em class="red">*</em>店铺名称</label>
                <input id="name" type="text" class="form-control"  placeholder="您的店铺的名字" value="${shop.name}">
            </div>
            <div class="form-group">
                <label><em class="red">*</em>店铺logo</label>
                <a class="upload-file-btn">
                    <i class="iconfont"></i>
                    <img id="logoImage" src="${shop.logo }" width="98" height="100"/>
                    <input id="logo" type="file" class="upload_file" onchange="ajaxFileUpload('/${user.loginName }/upload/shop','logo','logoImage')"/>
                </a>
            </div>
            <div class="form-group">
                <label><em class="red">*</em>微信收钱码</label>
                <a class="upload-file-btn">
                    <i class="iconfont"></i>
                    <img id="weixinQrCodeImage" src="${shop.weixinQrCode }" width="98" height="100"/>
                    <input id="weixinQrCode" type="file" class="upload_file" onchange="ajaxFileUpload('/${user.loginName }/upload/shop','weixinQrCode','weixinQrCodeImage')"/>
                </a>
            </div>
            <div class="form-group">
                <label><em class="red">*</em>支付宝收钱码</label>
                <a class="upload-file-btn">
                    <i class="iconfont"></i>
                    <img id="zhifubaoQrCodeImage" src="${shop.zhifubaoQrCode }" width="98" height="100"/>
                    <input id="zhifubaoQrCode" type="file" class="upload_file" onchange="ajaxFileUpload('/${user.loginName }/upload/shop','zhifubaoQrCode','zhifubaoQrCodeImage')"/>
                </a>
            </div>
            <div class="form-group">
                <label>所在省/市</label>
                <input id="province" type="text" class="form-control"  placeholder="店铺所在省/市">
            </div>
            <div class="form-group">
                <label>所在市/区</label>
                <input id="city" type="text" class="form-control"  placeholder="店铺所在市/区">
            </div>
            <div class="form-group">
                <label>详细地址</label>
                <input id="address" type="text" class="form-control"  placeholder="店铺详细地址">
            </div>
            <div class="form-group">
                <label>店铺描述（代购宣言）</label>
                <textarea id="description" class="form-control" rows="5"placeholder="请填写您的店铺描述或代购宣言">${shop.description}</textarea>
            </div>
        </div>
    </form>
    <div style="height: 60px"></div>
	<div class="new-brn-tab new-brn-border">
	    <div class="new-tbl-type">
	        <a id="saveShopBtn" class="new-tbl-pwd btn btn-primary">提交</a>
	        <a href="<%=basePath%>/admin/shop-info" class="new-tbl-pwd btn btn-cancel">取消</a>
	    </div>
	</div>
</section>
</div> 
</body>
</html>