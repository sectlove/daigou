<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/views/admin/app_base.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="format-detection" content="telephone=no" />
    <meta content="email=no" name="format-detection" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <%@ include file="/WEB-INF/views/admin/app_js_css.jsp"%>
    <script rel="script" src="<%=basePath%>/static/admin/app/js/password.js"></script>
    <title>修改密码</title>
</head>
<body>
<div  class="viewport">
<header>
    <div class="header-title">
        <a href="<%=basePath%>/admin/account" class="return"><i class="iconfont"></i>返回</a>
        <h2>修改密码</h2>
    </div>
</header>
<form role="form">
    <div class="password-box">
    	<div class="form-group">
            <label>旧密码</label>
            <input id="old_password" type="text" class="form-control"  placeholder="">
        </div>
        <div class="form-group">
            <label>新密码</label>
            <input id="new_password" type="text" class="form-control"  placeholder="">
        </div>
        <div class="form-group">
            <label>新密码确认</label>
            <input id="re_new_password" type="text" class="form-control"  placeholder="">
        </div>
    </div>
</form>
<div class="new-brn-tab new-brn-border">
    <div class="new-tbl-type">
        <a id="savePasswordBtn" class="new-tbl-pwd btn btn-primary">确认修改</a>
        <a href="<%=basePath%>/admin/account" class="new-tbl-pwd btn btn-cancel">取消</a>
    </div>
</div>
</div>
<script type="text/javascript">
    $(function() {
        var isIOS = (/iphoneipad/gi).test(navigator.appVersion);
        if (isIOS) {
            $(".password-box").on("focus", "input",function () {
                $(".new-brn-tab").addClass("pos-rel");
            }).on("focusout", "input",function () {
                $(".new-brn-tab").removeClass("pos-rel");
             });
        }
    });
 </script>
</body>
</html>