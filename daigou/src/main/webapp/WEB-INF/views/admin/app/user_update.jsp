<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/views/admin/app_base.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="format-detection" content="telephone=no" />
    <meta content="email=no" name="format-detection" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <%@ include file="/WEB-INF/views/admin/app_js_css.jsp"%>
    <!--引入 lrz 插件 用于压缩图片-->
    <script type="text/javascript" src="<%=basePath%>/static/admin/app/js/localResizeIMG/lrz.bundle.js"></script>
    <script rel="script" src="<%=basePath%>/static/admin/app/js/fileUpload.js"></script>
    <script rel="script" src="<%=basePath%>/static/admin/app/js/user_update.js"></script>
    <title>完善信息</title>
</head>
<body>
<div class="viewport">
<header>
	<div class="header-title">
        <a href="<%=basePath%>/admin/user-info" class="return"><i class="iconfont"></i>返回</a>
        <h2>编辑个人信息</h2>
    </div>
</header>   
<section>
<form id="userInfoForm" role="form">
    <div class="form-box">
        <div class="form-group">
            <label><em class="red">*</em>姓名</label>
            <input id="name" type="text" class="form-control"  placeholder="请填写您的姓名" value="${user.name }">
        </div>
        <div class="form-group">
            <label><em class="red">*</em>微信号</label>
            <input id="weixin" type="text" class="form-control"  placeholder="请输入您的微信号" value="${user.weixin }">
        </div>
        <div class="form-group">
            <label><em class="red">*</em>微信二维码</label>
            <a class="upload-file-btn">
                <i class="iconfont"></i>
                <img id="weixinQrCodeImage" src="${user.weixinQrCode }" width="98" height="100"/>
                <input id="weixinQrCode" type="file" class="upload_file" onchange="ajaxFileUpload('/${user.loginName }/upload/weixinQrCode','weixinQrCode','weixinQrCodeImage')"/>
            </a>
        </div>
        <div class="form-group">
            <label><em class="red">*</em>支付宝账号</label>
            <input id="zhifubao" type="text" class="form-control"  placeholder="请输入支付宝账号" value="${user.zhifubao }">
        </div>
        <div class="form-group">
            <label><em class="red">*</em>支付宝二维码</label>
            <a class="upload-file-btn">
                <i class="iconfont"></i>
                <img id="zhifubaoQrCodeImage" src="${user.zhifubaoQrCode }" width="98" height="100"/>
                <input id="zhifubaoQrCode" type="file" class="upload_file" onchange="ajaxFileUpload('/${user.loginName }/upload/zhifubaoQrCode','zhifubaoQrCode','zhifubaoQrCodeImage')"/>
            </a>
        </div>
        <div class="form-group">
            <label>邮箱</label>
            <input id="email" type="text" value="${user.email }" class="form-control"  placeholder="请输入邮箱以便接受系统消息">
        </div>
        <div class="form-group">
            <label>QQ账号</label>
            <input id="qq" type="text" value="${user.qq }" class="form-control"  placeholder="请输入QQ账号">
        </div>
        <div class="form-group">
            <label>性别</label>
            <select id="sex" class="form-control">
                <option value = "1" <c:if test="${user.sex == 1}">selected="selected"</c:if>>男</option>
                <option value = "0" <c:if test="${user.sex == 0}">selected="selected"</c:if>>女</option>
                <option value = "-1" <c:if test="${user.sex == -1}">selected="selected"</c:if>>保密</option>
            </select>
        </div>
        <div class="form-group">
            <label>身份证号</label>
            <input id="idCard" type="text" class="form-control"  placeholder="请输入您的身份证号" value="${user.idCard }">
        </div>
        <div class="form-group">
            <label>您的详细地址</label>
            <textarea id="address" class="form-control" rows="5" placeholder="请详细填写您的地址">${user.address }</textarea>
        </div>
    </div>
</form>
<div style="height: 60px"></div>
<div class="new-brn-tab new-brn-border">
    <div class="new-tbl-type">
        <a id="saveUserBtn" class="new-tbl-pwd btn btn-primary">提交</a>
        <a href="<%=basePath%>/admin/user-info" class="new-tbl-pwd btn btn-cancel">取消</a>
    </div>
</div>
</section>
</div>
</body>
</html>