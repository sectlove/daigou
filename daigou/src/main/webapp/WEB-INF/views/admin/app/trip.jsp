<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/views/admin/app_base.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <!--申明当前页面的编码集-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="format-detection" content="telephone=no" />
    <meta content="email=no" name="format-detection" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/base.css">
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/basic.css">
    <link href="<%=basePath%>/static/admin/app/css/iconfont.css" rel="stylesheet">
    <script rel="script" src="<%=basePath%>/static/admin/app/js/jquery-1.11.3.min.js"></script>
    <script rel="script" src="<%=basePath%>/static/admin/app/js/footer.js"></script>
    <title>行程管理</title>
</head>
<body style="background:#f4f4f4;">
<div class="viewport">
<header>
    <div class="header-title">
        <h2>我的行程</h2>
        <a href="<%=basePath%>/admin/trip/${trip.id}" class="news-a-brn">${tripButton}</a>
    </div>
</header>
<section>
    <div class="sales-box">
       <div class="sales-card">
			<div class="information-con">
				<c:if test="${not empty trip.targetAddress}">
				亲，您还没有行程，赶快添加，分享您的代购行程吧！！！
				</c:if>
				<c:if test="${not empty trip.targetAddress}">
				<div class="text-title-time" style="text-align:left">
		            <span class="author-name">目的地</span>
		            <span>${trip.targetAddress}</span>
				</div>
	           	<span>出发时间：<fmt:formatDate value="${trip.startDate}" pattern="yyyy-MM-dd"/></span></br>
	           	<span>返程时间：<fmt:formatDate value="${trip.endDate}" pattern="yyyy-MM-dd"/></span></br>
	           	<span>行程状态：${trip.statusName}</span></br>
		        <div class="article-con">
		           	<p>${trip.remark}</p>
		            <img src="${trip.tripPic}">
		            <img src="${trip.tripPic1}">
		            <img src="${trip.tripPic2}">
		            <img src="${trip.tripPic3}">
		            <img src="${trip.tripPic4}">
		            <img src="${trip.tripPic5}">
		            <img src="${trip.tripPic6}">
		        </div>
				</c:if>
		    </div>
       </div>
    </div>
</section>
</div>
<footer>
    <%@ include file="footer.jsp" %>
</footer>
</body>
</html>