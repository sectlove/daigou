<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/views/admin/app_base.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="format-detection" content="telephone=no" />
    <meta content="email=no" name="format-detection" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <%@ include file="/WEB-INF/views/admin/app_js_css.jsp"%>
    <!--引入 lrz 插件 用于压缩图片-->
    <script type="text/javascript" src="<%=basePath%>/static/admin/app/js/localResizeIMG/lrz.bundle.js"></script>
    <script rel="script" src="<%=basePath%>/static/admin/app/js/fileUpload.js"></script>
    <script rel="script" src="<%=basePath%>/static/admin/app/js/user_update.js"></script>
    <title>员工管理</title>
</head>
<body>
<div class="viewport">
<header>
	<div class="header-title">
        <a href="<%=basePath%>/admin/employee" class="return"><i class="iconfont"></i>返回</a>
        <h2>${employeeTitle }</h2>
    </div>
</header>   
<section>
<form id="userInfoForm" role="form">
    <div class="form-box">
        <div class="form-group">
            <label><em class="red">*</em>姓名</label>
            <input id="employeeId" type="hidden" value="${employee.id }">
            <input id="name" type="text" class="form-control"  placeholder="请填写员工姓名" value="${employee.name }">
        </div>
        <div class="form-group">
            <label><em class="red">*</em>手机号</label>
            <input id="loginName" type="text" class="form-control"  placeholder="请填写员工手机号" value="${employee.loginName }">
        </div>
        <c:if test="${empty employee.name }">
        <div class="form-group">
        	<label><em class="red">*</em>密码</label>
        	<input id="password" type="password" class="form-control"  placeholder="请输入密码">
	    </div>
	    <div class="form-group">
        	<label><em class="red">*</em>确认密码</label>
	        <input id="repassword" type="password" class="form-control"  placeholder="请再次输入密码">
	    </div>
        </c:if>
	    <div class="form-group">
            <label><em class="red">*</em>员工角色</label>
            <div class="row" >
	            <div class="col-xs-6">
					<input type="radio" name="businessType" value="2" <c:if test="${employee.type == 2}">checked</c:if>>经理
	            </div>
	            <div class="col-xs-6">
	            	<input type="radio" name="businessType" value="3" <c:if test="${employee.type == 3}">checked</c:if>>业务员
	            </div>
            </div>
        </div>
        <div id="leaderDiv" class="form-group">
            <label>上级经理</label>
            <div class="row" >
	            <div class="col-xs-10">
	            	<input id="leaderId" type="hidden" value="${employee.leaderId }">
            		<input id="leaderName" type="text" readonly class="form-control"  placeholder="请选择上级经理" value="${employee.leaderName }">
	            </div>
	            <div class="col-xs-2">
	            	<a id="clearLeaderBnt" class="new-tbl-cell btn btn-warning">清空</a>
	            </div>
            </div>
        </div>
        <div class="form-group">
            <label><em class="red">*</em>微信号</label>
            <input id="weixin" type="text" class="form-control"  placeholder="请输入员工微信号" value="${employee.weixin }">
        </div>
        <div class="form-group">
            <label><em class="red"></em>微信二维码</label>
            <a class="upload-file-btn">
                <i class="iconfont"></i>
                <img id="weixinQrCodeImage" src="${employee.weixinQrCode }" width="98" height="100"/>
                <input id="weixinQrCode" type="file" class="upload_file" onchange="ajaxFileUpload('/${user.loginName }/upload/weixinQrCode','weixinQrCode','weixinQrCodeImage')"/>
            </a>
        </div>
        <div class="form-group">
        	<label>邮箱</label>
	        <input id="email" type="text" class="form-control"  placeholder="请输入邮箱以便接收系统消息" value="${employee.email }">
	    </div>
        <div class="form-group">
            <label>QQ账号</label>
            <input id="qq" type="text" class="form-control"  placeholder="请输入QQ账号" value="${employee.qq }">
        </div>
        <div class="form-group">
            <label>性别</label>
            <select id="sex" class="form-control">
                <option value = "1" <c:if test="${employee.sex == 1}">selected="selected"</c:if>>男</option>
                <option value = "0" <c:if test="${employee.sex == 0}">selected="selected"</c:if>>女</option>
                <option value = "-1" <c:if test="${employee.sex == -1}">selected="selected"</c:if>>保密</option>
            </select>
        </div>
        <div class="form-group">
            <label>身份证号</label>
            <input id="idCard" type="text" class="form-control"  placeholder="请输入员工身份证号" value="${employee.idCard }">
        </div>
        <div class="form-group">
            <label>员工住址</label>
            <textarea id="address" class="form-control" rows="5" placeholder="请详细填写员工住址">${employee.address }</textarea>
        </div>
    </div>
</form>
<div style="height: 60px"></div>
<div class="new-brn-tab new-brn-border">
    <div class="new-tbl-type">
        <a id="saveEmployeeBtn" class="new-tbl-pwd btn btn-primary">提交</a>
        <a href="<%=basePath%>/admin/employee" class="new-tbl-pwd btn btn-cancel">取消</a>
    </div>
</div>
</section>
</div>
<div id="employeeListDiv" class="select-div-view">
	<header>
	    <div class="header-title">
	    	<a id="hiddenListBtn" class="return"><i class="iconfont"></i>返回</a>
	        <h2>请单击选择经理</h2>
	    </div>
	</header>
	<article>
	    <div class="date-tab-type">
	        <ul class="date-tab-cell">
	            <li>员工姓名</li>
	            <li>员工电话</li>
	            <li>员工微信</li>
	            <li>员工类型</li>
	        </ul>
	    </div>
	    <div id="detailBoxDiv" class="details-box">
	    	<c:forEach var="employee" items="${employeeList}">
	    	<a onclick="selectMyChoice('${employee.id }','${employee.name }')" class="details-link">
		        <ul class="details-list">
		            <li>${employee.name }</li>
		            <li>${employee.loginName }</li>
		            <li>${employee.weixin }</li>
		            <li>
		            	<c:if test="${employee.type == 2 }">经理</c:if>
	                	<c:if test="${employee.type == 3 }">业务员</c:if>
	               	</li>
		        </ul>
	    	</a>
	    	</c:forEach>
	    </div>
	</article>
</div>
</body>
</html>