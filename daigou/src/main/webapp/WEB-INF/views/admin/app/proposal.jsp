<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/views/admin/app_base.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="format-detection" content="telephone=no" />
    <meta content="email=no" name="format-detection" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/base.css">
    <link href="<%=basePath%>/static/admin/app/css/iconfont.css" rel="stylesheet">
    <script rel="script" src="<%=basePath%>/static/admin/app/js/jquery-1.11.3.min.js"></script>
    <title>求评价建议</title>
</head>
<body>
<div class="viewport">
<header>
    <div class="header-title">
      <a href="<%=basePath%>/admin/account" class="return"><i class="iconfont"></i>返回</a>
      <h2>评价建议</h2>
    </div>
</header>
<section>
    <div class="new-details-box">
        <ul class="new-details-list">
            <div class="step-type">建议意见</div>
            <div class="step-type-details">
                <textarea class="step-type-control " rows="5" placeholder="请输入建议意见,并提交,建议被采纳后,我们将有红包奖励！"></textarea>
            </div>
            <div class="step-btn-box">
                 <a href="#" class="step-btn">提交</a>
            </div>
        </ul>
    </div>
</section>
</div>
</body>
</html>