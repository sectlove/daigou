<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/views/admin/app_base.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="format-detection" content="telephone=no" />
    <meta content="email=no" name="format-detection" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/base.css">
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/claim.css">
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/bootstrap.min.css">
    <link href="<%=basePath%>/static/admin/app/css/iconfont.css" rel="stylesheet">
    <script rel="script" src="<%=basePath%>/static/admin/app/js/jquery-1.11.3.min.js"></script>
    <script rel="script" src="<%=basePath%>/static/admin/app/js/footer.js"></script>
    <script rel="script" src="<%=basePath%>/static/admin/app/js/employee_list.js"></script>
    <script> var baselocation='<%=basePath%>/admin';</script>
	<script> var imagelocation='<%=basePath%>/';</script>
    <title>员工管理</title>
</head>
<body>
<div class="viewport">
<header>
    <div class="header-title">
    	<a href="<%=basePath%>/admin/account" class="return"><i class="iconfont"></i>返回</a>
        <h2>员工列表</h2>
        <a href="<%=basePath%>/admin/employee/0" class="news-a-brn">新增</a>
    </div>
</header>
<form role="form">
    <div class="form-group form-box">
        <a id="searchBtn" class="icon-search"><i class="iconfont"></i></a>
        <input id="searchInput" type="text" class="form-control input-text" placeholder="姓名，电话">
    </div>
</form>
</br>
<article>
    <div class="date-tab-type">
        <ul class="date-tab-cell">
            <li>员工姓名</li>
            <li>员工电话</li>
            <li>员工微信</li>
            <li>员工类型</li>
        </ul>
    </div>
    <div id="detailBoxDiv" class="details-box">
    	<c:forEach var="employee" items="${employeeList}">
    	<a href="<%=basePath%>/admin/employee/${employee.id }/details" class="details-link">
	        <ul class="details-list">
	            <li>${employee.name }</li>
	            <li>${employee.loginName }</li>
	            <li>${employee.weixin }</li>
	            <li>
	            	<c:if test="${employee.type == 2 }">经理</c:if>
                	<c:if test="${employee.type == 3 }">业务员</c:if>
               	</li>
	        </ul>
    	</a>
    	</c:forEach>
    </div>
</article>
</div>
</body>
</html>