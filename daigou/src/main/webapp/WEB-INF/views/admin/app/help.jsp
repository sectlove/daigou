<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="format-detection" content="telephone=no" />
    <meta content="email=no" name="format-detection" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/base.css">
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/basic.css">
    <link href="<%=basePath%>/static/admin/app/css/iconfont.css" rel="stylesheet">
    <script rel="script" src="<%=basePath%>/static/admin/app/js/jquery-1.11.3.min.js"></script>
    <style>
        *{margin: 0; padding: 0}
        .help-container{
            max-width: 100%;
            margin-left: auto;
            margin-right: auto;
            background: #f0f0f0;
        }
        .my-biji-view{
            margin: 0;
            padding: 0;
            list-style-type: none;
        }
        .my-biji-view li{
            border-top: 10px solid #fff;
            padding: 11px 15px;
        }
        .my-biji-view li .biji-tit{
            height: 20px;
            line-height: 20px;
            font-size: 14px;
            color: #000;
            font-weight: 600;
            margin-bottom: 10px;
            overflow: hidden;
        }
        .my-biji-view li .biji-content{
            font-size: 14px;
            color: #000;
            line-height: 20px;
            margin-bottom: 10px;
        }
        .my-biji-view li .biji-oth{
            border-top: 1px solid #f2f2f2;
            padding: 10px 0;
            display: flex;
            align-items: center;
            justify-content: space-between;
            font-size: 12px;
            color: #999;
        }
        button{
            padding: 3px 6px;
            border: none;
            outline: none;
            background-color: #cc0000;
            color: #fff;
            font-size: 12px;
            border-radius: 4px;
        }
    </style>
    <title>帮助信息</title>
</head>
<body>
<div  class="viewport">
<header>
    <div class="header-title">
        <a href="<%=basePath%>/admin/account" class="return"><i class="iconfont"></i>返回</a>
        <h2>帮助文档</h2>
    </div>
</header>
<article>
    <div class="text-box content-before">
        <div class="help-container">
	        <ul class="my-biji-view">
	            <li>
	                <h4 class="biji-tit">
	                    1.如何新增商品？
	                </h4>
	                <div class="biji-content">
	                    <b>1.如何新增商品？</b><br/>
	                    <b>1.1.找到商品管理页面</b><br/><br/>
	                   	<img alt="" width="100%" height="100%" src="<%=basePath%>/static/admin/app/images/help_product_list.jpg">
	                   	<b>1.2.点击商品管理的新增,跳转到商品新增页面</b><br/><br/>
	                   	<img alt="" width="100%" height="100%" src="<%=basePath%>/static/admin/app/images/help_product_add.jpg">
	                </div>
	                <a class="biji-oth">
	                    <time></time>
	                    <button>展开</button>
	                </a>
	            </li>
	            <li>
	                <h4 class="biji-tit">
	                    2.如何管理我的商品？
	                </h4>
	                <div class="biji-content">
	                    <b>2.如何管理我的商品？</b><br/><br/>
	                    <b>2.1.点击商品列表中的商品</b><br/><br/>
	                    <img alt="" width="100%" height="100%" src="<%=basePath%>/static/admin/app/images/help_product_list2.jpg">
	                    <b>2.2.查看商品详情</b><br/><br/>
	                    <img alt="" width="100%" height="100%" src="<%=basePath%>/static/admin/app/images/help_product_detail.jpg">
	                    <b>2.3.点击编辑进入商品编辑页</b><br/><br/>
	                    <img alt="" width="100%" height="100%" src="<%=basePath%>/static/admin/app/images/help_product_edit.jpg">
	                </div>
	                <a class="biji-oth">
	                    <time></time>
	                    <button>展开</button>
	                </a>
	            </li>
	            <li>
	                <h4 class="biji-tit">
	                    3.如何查看我的订单？
	                </h4>
	                <div class="biji-content">
	                    <b>3.如何查看我的订单？</b><br/><br/>
	                    <b>3.1.首页,显示我的新建订单和最新支付的订单</b><br/><br/>
	                    <img alt="" width="100%" height="100%" src="<%=basePath%>/static/admin/app/images/help_index.jpg">
	                    <b>3.2.进入我的订单页面</b><br/><br/>
	                    <img alt="" width="100%" height="100%" src="<%=basePath%>/static/admin/app/images/help_order_list.jpg">
	                    <b>3.3.通过条件查询我的订单</b><br/><br/>
	                    <img alt="" width="100%" height="100%" src="<%=basePath%>/static/admin/app/images/help_order_search1.png">
	                    <b>3.4.通过状态查询我的订单</b><br/><br/>
	                    <img alt="" width="100%" height="100%" src="<%=basePath%>/static/admin/app/images/help_order_search2.png">
	                </div>
	                <a class="biji-oth">
	                    <time></time>
	                    <button>展开</button>
	                </a>
	            </li>
	            <li>
	                <h4 class="biji-tit">
	                    4.如何管理我的订单？
	                </h4>
	                <div class="biji-content">
	                    <b>4.如何管理我的订单？</b><br/><br/>
	                    <img alt="" width="100%" height="100%" src="<%=basePath%>/static/admin/app/images/help_order_detail.jpg">
	                    <img alt="" width="100%" height="100%" src="<%=basePath%>/static/admin/app/images/help_order_pay.png">
	                </div>
	                <a class="biji-oth">
	                    <time></time>
	                    <button>展开</button>
	                </a>
	            </li>
	            <li>
	                <h4 class="biji-tit">
	                    5.如何代客户下单？
	                </h4>
	                <div class="biji-content">
	                    <b>5.如何代客户下单？</b><br/><br/>
	                    <img alt="" width="100%" height="100%" src="<%=basePath%>/static/admin/app/images/help_order_add.jpg">
	                </div>
	                <a class="biji-oth">
	                    <time></time>
	                    <button>展开</button>
	                </a>
	            </li>
	            <li>
	                <h4 class="biji-tit">
	                    6.如何添加广告宣传自己？
	                </h4>
	                <div class="biji-content">
	                    <b>6.如何添加广告宣传自己？</b><br/><br/>
	                    <img alt="" width="100%" height="100%" src="<%=basePath%>/static/admin/app/images/help_advert.jpg">
	                </div>
	                <a class="biji-oth">
	                    <time></time>
	                    <button>展开</button>
	                </a>
	            </li>
	            <li>
	                <h4 class="biji-tit">
	                    7.如何修改个人信息？
	                </h4>
	                <div class="biji-content">
	                    <b>7.如何修改个人信息？</b><br/><br/>
	                    <img alt="" width="100%" height="100%" src="<%=basePath%>/static/admin/app/images/help_user.jpg">
	                </div>
	                <a class="biji-oth">
	                    <time></time>
	                    <button>展开</button>
	                </a>
	            </li>
	            <li>
	                <h4 class="biji-tit">
	                    8.如何修改店铺信息？
	                </h4>
	                <div class="biji-content">
	                    <b>8.如何修改店铺信息？</b><br/><br/>
	                    <img alt="" width="100%" height="100%" src="<%=basePath%>/static/admin/app/images/help_shop.jpg">
	                </div>
	                <a class="biji-oth">
	                    <time></time>
	                    <button>展开</button>
	                </a>
	            </li>
	            <li>
	                <h4 class="biji-tit">
	                    9.如何查看财务报表？
	                </h4>
	                <div class="biji-content">
	                    <b>9.如何查看财务报表？</b><br/><br/>
	                    <img alt="" width="100%" height="100%" src="<%=basePath%>/static/admin/app/images/help_report.jpg">
	                </div>
	                <a class="biji-oth">
	                    <time></time>
	                    <button>展开</button>
	                </a>
	            </li>
	        </ul>
	    </div>
    </div>
</article>
</div>
</body>
<script>
    $(function(){
        $(".biji-content").hide();
        //按钮点击事件
        $("button").click(function(){
            var txts = $(this).parents("li");
            if ($(this).text() == "展开"){
                $(this).text("收起");
                txts.find(".biji-tit").hide();
                txts.find(".biji-content").show();
            }else{
                $(this).text("展开");
                txts.find(".biji-tit").show();
                txts.find(".biji-content").hide();
            }
        })
    });
</script>
</html>
