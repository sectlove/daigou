<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/views/admin/app_base.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="format-detection" content="telephone=no" />
    <meta content="email=no" name="format-detection" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/base.css">
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/claim.css">
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/bootstrap.min.css">
    <link href="<%=basePath%>/static/admin/app/css/iconfont.css" rel="stylesheet">
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/iscroll.css">
    <script rel="script" src="<%=basePath%>/static/admin/app/js/jquery-1.11.3.min.js"></script>
    <script rel="script" src="<%=basePath%>/static/admin/app/js/footer.js"></script>
    <script rel="script" src="<%=basePath%>/static/admin/app/js/product_list.js"></script>
    <script> var baselocation='<%=basePath%>/admin';</script>
	<script> var imagelocation='<%=basePath%>/';</script>
    <title>商品管理</title>
</head>
<body>
<header>
    <div class="header-title">
    	<a href="<%=basePath%>/admin/order/0" class="return"><i class="iconfont"></i>返回</a>
        <h2>商品列表</h2>
        <%-- <a href="<%=basePath%>/admin/product/0" class="news-a-brn">新增</a> --%>
    </div>
</header>
<form role="form">
    <div class="form-group form-box">
        <a id="searchBtn" class="icon-search"><i class="iconfont"></i></a>
        <input id="searchInput" type="text" class="form-control input-text" id="name" placeholder="商品名,品牌,类别,规格,产地">
    </div>
</form>
<!--代码部分begin-->
<div id="wrapper">
	<div id="scroller">
		<div id="scroller-pullDown">
        	  <span id="down-icon" class="icon-double-angle-down pull-down-icon"></span>
        	  <span id="pullDown-msg" class="pull-down-msg">下拉刷新</span>		
        </div>
		<div id="scroller-content">
			<article>
				<div class="row">
			        <ul class="claim-title-type">
			            <li id="totalCountLi" class="col-xs-4">商品总数：${totalCount}</li>
			            <li id="onSaleCountLi" class="col-xs-4">上架:${onSaleCount}</li>
			            <li id="offSaleCountLi" class="col-xs-4">下架:${offSaleCount}</li>
			        </ul>
			    </div>
			    <div class="date-tab-type">
			        <ul class="date-tab-cell">
			            <li>商品图片</li>
			            <li>名称  / 规格</li>
			            <li>价格</li>
			            <li>状态</li>
			        </ul>
			    </div>
			    <div id="detailBoxDiv" class="details-box">
			    	<c:if test="${empty productList}"><span>您暂时还没有售卖的商品,去新建吧！！！</span></c:if>
			    	<c:forEach var="product" items="${productList}">
				        <a href="<%=basePath%>/admin/order/0_${product.id }" class="details-link">
				            <ul class="details-list-image">
				                <li>
			               			<img id="picMainImage" style="margin: 0 auto;" src="${product.picMain }" width="73.5" height="75"/>
			               		</li>
				                <li>${product.name }</br>${product.spec }</li>
				                <li><s>￥${product.marketPrice }</s></br>￥${product.salePrice }</li>
				                <li>${product.statusName }</li>
				            </ul>
				        </a>
			    	</c:forEach>
			    </div>
			</article>
		</div>
	    <div id="scroller-pullUp">
			<span id="up-icon" class="icon-double-angle-up pull-up-icon"></span>
		    <span id="pullUp-msg" class="pull-up-msg">上拉加载更多</span>
        </div>
	</div>
</div>
<div class="add-con">
	<%-- <a href="<%=basePath%>/admin/product/0" class="add-btn"><i class="iconfont"></i>新增</a> --%>
</div>
<div style="height: 60px"></div>
</body>
	<script src="<%=basePath%>/static/admin/app/js/iscroll.js"></script>
	<script src="<%=basePath%>/static/admin/app/js/product_loading.js"></script>
	<script type="text/javascript">
	loaded ('order');
	</script>
</html>