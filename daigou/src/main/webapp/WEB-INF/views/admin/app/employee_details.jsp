<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/views/admin/app_base.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="format-detection" content="telephone=no" />
    <meta content="email=no" name="format-detection" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/base.css">
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/basic.css">
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/bootstrap.min.css">
    <link href="<%=basePath%>/static/admin/app/css/iconfont.css" rel="stylesheet">
    <script rel="script" src="<%=basePath%>/static/admin/app/js/jquery-1.11.3.min.js"></script>
    <title>员工管理</title>
</head>
<body>
<div class="viewport">
<header>
    <div class="header-title">
        <a href="<%=basePath%>/admin/employee" class="return"><i class="iconfont"></i>返回</a>
        <h2>员工信息</h2>
        <a href="<%=basePath%>/admin/employee/${employee.id }" class="news-a-brn">编辑</a>
    </div>
</header>
<article>
    <div class="info-con">
        <h2 class="info-title-font-size">个人信息(${employee.name })</h2>
        <ul>
            <li>
                <span>手机号：</span>
                <span>${employee.mobile }</span>
            </li>
            <li>
                <span>邮箱：</span>
                <span>${employee.email }</span>
            </li>
            <li>
                <span>性别：</span>
                <span>
                	<c:if test="${employee.sex == 1 }">男</c:if>
                	<c:if test="${employee.sex == 0 }">女</c:if>
                	<c:if test="${employee.sex == -1 }">保密</c:if>
                </span>
            </li>
            <li>
                <span>员工角色：</span>
                <span>
                	<c:if test="${employee.type == 2 }">经理</c:if>
                	<c:if test="${employee.type == 3 }">业务员</c:if>
                </span>
            </li>
            <li>
                <span>上级经理：</span>
                <span>${employee.leaderName }</span>
            </li>
        </ul>
    </div>
    <div class="info-con">
        <h2 class="info-title-font-size">重置密码</h2>
        <ul>
            <li>
                <span><a id="resetPwdBnt" class="new-tbl-cell btn btn-warning">重置密码</a></span>
                <span>重置后的默认密码:123456</span>
            </li>
        </ul>
    </div>
    <div class="info-con">
        <h2 class="info-title-font-size">账号信息</h2>
        <ul>
        	<li>
                <span>微信号：</span>
                <span>${employee.weixin }</span>
            </li>
            <li>
                <span>微信二维码：</span>
                <span>
	                <a href="${employee.weixinQrCode }">
	                	<img id="weixinQrCodeImage" src="${employee.weixinQrCode }" width="98" height="100"/>
	                </a>
                </span>
            </li>
            <li>
                <span>QQ账号：</span>
                <span>${employee.qq }</span>
            </li>
        </ul>
    </div>
    <div class="info-con">
        <h2 class="info-title-font-size">实名认证</h2>
        <ul>
            <li>
                <span>身份证号：</span>
                <span>${employee.idCard }</span>
            </li>
            <li>
                <span>地址：</span>
                <span>${employee.address }</span>
            </li>
        </ul>
    </div>
</article>
</div>
</body>
</html>
