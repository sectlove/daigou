<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/views/shop/app/app_base.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui"/>
	<meta name="apple-mobile-web-app-capable" content="yes"/>
	<meta name="apple-mobile-web-app-status-bar-style" content="black"/>
	<meta name="format-detection" content="telephone=no, email=no"/>
	<meta charset="UTF-8">
	<title>我的订单</title>
	<link rel="stylesheet" href="<%=basePath%>/static/shop/app/css/core.css">
	<link rel="stylesheet" href="<%=basePath%>/static/shop/app/css/icon.css">
	<link rel="stylesheet" href="<%=basePath%>/static/shop/app/css/home.css">
	<link rel="stylesheet" href="<%=basePath%>/static/admin/app/js/layer/skin/layer.css">
	<link rel="shortcut icon" href="<%=basePath%>/favicon.ico" type="image/x-icon" />
	<link href="<%=basePath%>/favicon.ico" sizes="114x114" rel="apple-touch-icon-precomposed">
	<script> var baselocation='<%=basePath%>';</script>

</head>
<body>

	<header class="aui-header-default aui-header-fixed aui-header-bg">
		<a href="javascript:history.back(-1)" class="aui-header-item">
			<i class="aui-icon aui-icon-back-white"></i>
		</a>
		<div class="aui-header-center aui-header-center-clear">
			<div class="aui-header-center-logo">
				<div class="aui-car-white-Typeface">我的订单</div>
			</div>
		</div>
		<a href="#" class="aui-header-item-icon"   style="min-width:0">
			<i class="aui-icon aui-icon-search"></i>
		</a>
	</header>

	<section class="aui-myOrder-content">
		<div class="m-tab demo-small-pitch" data-ydui-tab>
			<div class="aui-myOrder-fix">
				<ul class="tab-nav">
					<li class="tab-nav-item tab-active"><a href="javascript:;">全部</a></li>
					<li class="tab-nav-item"><a href="javascript:;">待付款</a></li>
					<li class="tab-nav-item"><a href="javascript:;">待发货</a></li>
					<li class="tab-nav-item"><a href="javascript:;">待收货</a></li>
					<li class="tab-nav-item"><a href="javascript:;">完成/取消</a></li>
				</ul>
			</div>
			<div class="aui-prompt"><i class="aui-icon aui-prompt-sm"></i>如订单调整,请及时联系店家!!!</div>
			<div class="tab-panel">
				<div class="tab-panel-item tab-active">
					<c:if test="${empty orderList}"><span>您暂时还没有订单,加油哦！！！</span></c:if>
					<ul>
    				<c:forEach var="order" items="${orderList}">
						<li>
							<div class="aui-list-title-info">
								<a href="#" class="aui-well ">
									<div class="aui-well-bd"><fmt:formatDate value="${order.createdTime }" pattern="yyyy-MM-dd HH:mm:ss"/></div>
									<div class="aui-well-ft">${order.statusName}</div>
								</a>
								<a href="<%=basePath%>/order/${order.id}/details" class="aui-list-product-fl-item">
									<div class="aui-list-product-fl-img">
										<div class="order_contain">
											<img id="order_${order.id }" src="${order.picMain}" alt="">
										</div>
									</div>
									<div class="aui-list-product-fl-text">
										<h3 class="aui-list-product-fl-title">${order.productName}</h3>
										<div class="aui-list-product-fl-mes">
											<div>
												<span class="aui-list-product-item-price">
													<em>¥</em>
													${order.price}
												</span>
											</div>
											<div class="aui-btn-purchase">
												x${order.num}
											</div>
										</div>
										<div class="aui-list-product-fl-mes">
											<div>
												<span class="aui-list-product-item-price">
													<em>¥</em>
													${order.totalAmount}(总额)
												</span>
											</div>
											<div class="aui-btn-purchase">
												${order.payStatusName}
											</div>
										</div>
									</div>

								</a>
							</div>
							<div class="aui-list-title-btn">
								<!-- 订单状态是新建且支付状态不是店主已确认状态，则可以取消订货 -->
								<c:if test="${order.status == 1 and order.payStatus == 0}">
								<a onclick="updateOrderStatus('${order.id}','${order.ts}','7')">取消订货</a>
								</c:if>
								<c:if test="${order.status != 6 and order.status != 7 and (order.payStatus == 0 or order.payStatus == 1)}">
								<a href="<%=basePath%>/order/${order.id }/pay" class="red-color">继续付款</a>
								</c:if>
								<c:if test="${order.dr == 1 and order.status == 3}">
					            <a onclick="updateOrderStatus('${order.id}','${order.ts}','4')" class="red-color">确认收货</a>
								</c:if>
								<c:if test="${order.dr == 1 and order.status == 4}">
					            <a onclick="updateOrderStatus('${order.id}','${order.ts}','5')" class="btn btn-primary btn-sm">订单完成</a>
								</c:if>
							</div>
							<div class="aui-dri"></div>
						</li>
    				</c:forEach>
					</ul>
				</div>
				<div class="tab-panel-item">
					<c:if test="${empty orderList1}"><span>您暂时还没有待付款订单,加油哦！！！</span></c:if>
					<ul>
						<c:forEach var="order" items="${orderList1}">
						<li>
							<div class="aui-list-title-info">
								<a href="#" class="aui-well ">
									<div class="aui-well-bd"><fmt:formatDate value="${order.createdTime }" pattern="yyyy-MM-dd HH:mm:ss"/></div>
									<div class="aui-well-ft">${order.statusName}</div>
								</a>
								<a href="<%=basePath%>/order/${order.id}/details" class="aui-list-product-fl-item">
									<div class="aui-list-product-fl-img">
										<div class="order_contain">
											<img id="order_${order.id }" src="${order.picMain}" alt="">
										</div>
									</div>
									<div class="aui-list-product-fl-text">
										<h3 class="aui-list-product-fl-title">${order.productName}</h3>
										<div class="aui-list-product-fl-mes">
											<div>
												<span class="aui-list-product-item-price">
													<em>¥</em>
													${order.price}
												</span>
											</div>
											<div class="aui-btn-purchase">
												x${order.num}
											</div>
										</div>
										<div class="aui-list-product-fl-mes">
											<div>
												<span class="aui-list-product-item-price">
													<em>¥</em>
													${order.totalAmount}(总额)
												</span>
											</div>
											<div class="aui-btn-purchase">
												${order.payStatusName}
											</div>
										</div>
									</div>
								</a>
							</div>
							<div class="aui-list-title-btn">
								<!-- 订单状态是新建且支付状态不是店主已确认状态，则可以取消订货 -->
								<c:if test="${order.status == 1 and order.payStatus == 0}">
								<a  onclick="updateOrderStatus('${order.id}','${order.ts}','7')">取消订货</a>
								</c:if>
								<c:if test="${order.status != 6 and order.status != 7 and (order.payStatus == 0 or order.payStatus == 1)}">
								<a href="<%=basePath%>/order/${order.id }/pay" class="red-color">继续付款</a>
								</c:if>
							</div>
							<div class="aui-dri"></div>
						</li>
						</c:forEach>
					</ul>
				</div>
				<div class="tab-panel-item">
					<c:if test="${empty orderList2}"><span>您暂时还没有待发货订单,加油哦！！！</span></c:if>
					<ul>
						<c:forEach var="order" items="${orderList2}">
						<li>
							<div class="aui-list-title-info">
								<a href="#" class="aui-well ">
									<div class="aui-well-bd"><fmt:formatDate value="${order.createdTime }" pattern="yyyy-MM-dd HH:mm:ss"/></div>
									<div class="aui-well-ft">${order.statusName}</div>
								</a>
								<a href="<%=basePath%>/order/${order.id}/details" class="aui-list-product-fl-item">
									<div class="aui-list-product-fl-img">
										<div class="order_contain">
											<img id="order_${order.id }" src="${order.picMain}" alt="">
										</div>
									</div>
									<div class="aui-list-product-fl-text">
										<h3 class="aui-list-product-fl-title">${order.productName}</h3>
										<div class="aui-list-product-fl-mes">
											<div>
												<span class="aui-list-product-item-price">
													<em>¥</em>
													${order.price}
												</span>
											</div>
											<div class="aui-btn-purchase">
												x${order.num}
											</div>
										</div>
										<div class="aui-list-product-fl-mes">
											<div>
												<span class="aui-list-product-item-price">
													<em>¥</em>
													${order.totalAmount}(总额)
												</span>
											</div>
											<div class="aui-btn-purchase">
												${order.payStatusName}
											</div>
										</div>
									</div>
								</a>
							</div>
							<div class="aui-list-title-btn">
							</div>
							<div class="aui-dri"></div>
						</li>
						</c:forEach>
					</ul>
				</div>
				<div class="tab-panel-item">
					<c:if test="${empty orderList3}"><span>您暂时还没有待收货订单,加油哦！！！</span></c:if>
					<ul>
						<c:forEach var="order" items="${orderList3}">
						<li>
							<div class="aui-list-title-info">
								<a href="#" class="aui-well ">
									<div class="aui-well-bd"><fmt:formatDate value="${order.createdTime }" pattern="yyyy-MM-dd HH:mm:ss"/></div>
									<div class="aui-well-ft">${order.statusName}</div>
								</a>
								<a href="<%=basePath%>/order/${order.id}/details" class="aui-list-product-fl-item">
									<div class="aui-list-product-fl-img">
										<div class="order_contain">
											<img id="order_${order.id }" src="${order.picMain}" alt="">
										</div>
									</div>
									<div class="aui-list-product-fl-text">
										<h3 class="aui-list-product-fl-title">${order.productName}</h3>
										<div class="aui-list-product-fl-mes">
											<div>
												<span class="aui-list-product-item-price">
													<em>¥</em>
													${order.price}
												</span>
											</div>
											<div class="aui-btn-purchase">
												x${order.num}
											</div>
										</div>
										<div class="aui-list-product-fl-mes">
											<div>
												<span class="aui-list-product-item-price">
													<em>¥</em>
													${order.totalAmount}(总额)
												</span>
											</div>
											<div class="aui-btn-purchase">
												${order.payStatusName}
											</div>
										</div>
									</div>
								</a>
								<a href="#" class="aui-well ">
									<div class="aui-well-bd">快递公司：${order.sendCompany}</div>
									<div class="aui-well-ft">发货时间：<fmt:formatDate value="${order.sendTime }" pattern="yyyy-MM-dd"/></div>
								</a>
								<a href="#" class="aui-well ">
									<div class="aui-well-bd">发货单号：${order.sendNo}</div>
								</a>
							</div>
							<div class="aui-list-title-btn">
								<c:if test="${order.dr == 1 and order.status == 3}">
					            <a  onclick="updateOrderStatus('${order.id}','${order.ts}','4')" class="red-color">确认收货</a>
								</c:if>
							</div>
							<div class="aui-dri"></div>
						</li>
						</c:forEach>
					</ul>
				</div>
				<div class="tab-panel-item">
					<c:if test="${empty orderList4}"><span>您暂时还没有已完成订单,加油哦！！！</span></c:if>
					<ul>
						<c:forEach var="order" items="${orderList4}">
						<li>
							<div class="aui-list-title-info">
								<a href="#" class="aui-well ">
									<div class="aui-well-bd"><fmt:formatDate value="${order.createdTime }" pattern="yyyy-MM-dd HH:mm:ss"/></div>
									<div class="aui-well-ft">${order.statusName}</div>
								</a>
								<a href="<%=basePath%>/order/${order.id}/details" class="aui-list-product-fl-item">
									<div class="aui-list-product-fl-img">
										<div class="order_contain">
											<img id="order_${order.id }" src="${order.picMain}" alt="">
										</div>
									</div>
									<div class="aui-list-product-fl-text">
										<h3 class="aui-list-product-fl-title">${order.productName}</h3>
										<div class="aui-list-product-fl-mes">
											<div>
												<span class="aui-list-product-item-price">
													<em>¥</em>
													${order.price}
												</span>
											</div>
											<div class="aui-btn-purchase">
												x${order.num}
											</div>
										</div>
										<div class="aui-list-product-fl-mes">
											<div>
												<span class="aui-list-product-item-price">
													<em>¥</em>
													${order.totalAmount}(总额)
												</span>
											</div>
											<div class="aui-btn-purchase">
												${order.payStatusName}
											</div>
										</div>
									</div>
								</a>
							</div>
							<div class="aui-list-title-btn">
								<c:if test="${order.dr == 1 and order.status == 4}">
					            <a  onclick="updateOrderStatus('${order.id}','${order.ts}','5')" class="btn btn-primary btn-sm">订单完成</a>
								</c:if>
							</div>
							<div class="aui-dri"></div>
						</li>
						</c:forEach>
					</ul>
				</div>
			</div>
		</div>
	</section>


	<script type="text/javascript" src="<%=basePath%>/static/shop/app/js/jquery.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>/static/shop/app/js/aui.js"></script>
	<script type="text/javascript" src="<%=basePath%>/static/shop/app/js/my_pic_view.js"></script>
	<script rel="script" src="<%=basePath%>/static/admin/app/js/layer/layer.js"></script>
	<script type="text/javascript" src="<%=basePath%>/static/shop/app/js/order.js"></script>
	<script type="text/javascript">
	window.onload=function () {
	    //显示商品图
	    show_pic_view_all('75','75','order_');
	}
	</script>
	<script type="text/javascript" >
        /**
         * Javascript API调用Tab
         */
        !function ($) {
            var $tab = $('#J_Tab');

            $tab.tab({
                nav: '.tab-nav-item',
                panel: '.tab-panel-item',
                activeClass: 'tab-active'
            });

			/*
			 $tab.find('.tab-nav-item').on('open.ydui.tab', function (e) {
			 console.log('索引：%s - [%s]正在打开', e.index, $(this).text());
			 });
			 */

            $tab.find('.tab-nav-item').on('opened.ydui.tab', function (e) {
                console.log('索引：%s - [%s]已经打开了', e.index, $(this).text());
            });
        }(jQuery);
	</script>

</body>
</html>