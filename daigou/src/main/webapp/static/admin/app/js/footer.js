/**
 * Created by admin on 2017/4/27.
 */
$(document).ready(function(){
    $(function () {
        if (String(window.location.pathname).indexOf("/admin/product") >= 0) {
            $("#cao-gao").removeClass("cao-gao"); //清除当前的灰色的图标class
            $("#cao-gao").addClass("cao-gao-active");//添加彩色的图标的class
        }
        else if (String(window.location.pathname).indexOf("/admin/order") >= 0)
        {
            $("#suo-pei").removeClass("suo-pei"); //清除当前的灰色的图标class
            $("#suo-pei").addClass("suo-pei-active");//添加彩色的图标的class
        }
        else if (String(window.location.pathname).indexOf("/admin/go-go") >= 0) {
            $("#news").removeClass("news"); //清除当前的灰色的图标class
            $("#news").addClass("news-active");//添加彩色的图标的class
        }
        else if (String(window.location.pathname).indexOf("/admin/account") >= 0) {
            $("#account").removeClass("account"); //清除当前的灰色的图标class
            $("#account").addClass("account-active");//添加彩色的图标的class
        }
        else if (String(window.location.pathname).indexOf("/admin") >= 0) {
            $("#index").removeClass("index"); //清除当前的灰色的图标class
            $("#index").addClass("index-active");//添加彩色的图标的class
        }
    })
});
