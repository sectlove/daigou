//表单验证
$(function(){
	function listEmployee(){
		//获取form的值
		var searchKey = $('#searchInput').val();
		
		$.ajax({
			url:baselocation+'/employee-query',
			//contentType:'application/json',
			type:'get',
			dataType:'json',
			data: {searchKey:searchKey},
			success:function(result){
				if(result.success==false){
					layer.msg(result.message,{icon:2,time:2000});
				}else{
					var employeeList = result.data;
					var jsonStr = "";
					if(employeeList.length > 0){
						for(var i=0;i<employeeList.length;i++){
							var employee = employeeList[i];
							jsonStr += "<a href='"+baselocation+"/employee/"+employee.id+"/details' class='details-link'>";
							jsonStr += "<ul class='details-list'>";
							jsonStr += "<li>"+ employee.name +"</li>";
							jsonStr += "<li>"+employee.loginName+"</li>";
							jsonStr += "<li>"+employee.weixin+"</li>";
							if(employee.type == 2){
								jsonStr += "<li>经理</li>";
							}else{
								jsonStr += "<li>业务员</li>";
							}
							jsonStr += "</ul>";
							jsonStr += "</a>";
						}
					}
					
					$("#detailBoxDiv").html(jsonStr);
				}
			},
			error:function(result){
				alert("系统异常,请联系管理员！");
			}
		});
	}
	
	$('#searchBtn').click(function () {
		listEmployee();
	})
	$('#searchInput').blur(function () {
		listEmployee();
	})
});