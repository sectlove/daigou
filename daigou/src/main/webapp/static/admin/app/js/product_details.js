//表单验证
$(function(){
	function updateProduct(status){
		//获取form的值
		var id = $('#id').val();
		var product = {};
		product["id"] = id;
		product["status"] = status;
		
		$.ajax({
			url:baselocation+'/product-update-status',
			contentType:'application/json',
			type:'post',
			dataType:'json',
			data: JSON.stringify(product),
			success:function(result){
				if(result.success==false){
					layer.msg(result.message,{icon:2,time:2000});
				}else{
					window.location.href=baselocation+'/product/'+id+'/details';
				}
			},
			error:function(result){
				alert("系统异常,请联系管理员！");
			}
		});
	}
	
	$('#onSaleBnt').click(function () {
		updateProduct(1);
	})
	
	$('#offSaleBnt').click(function () {
		updateProduct(0);
	})
	$('#deleteBnt').click(function () {
		layer.confirm('是否确认删除商品？', {
		    btn: ['确认','取消'], //按钮
		    icon: 3, 
		    title:'操作提示',
		    skin: 'layui-layer-demo', //样式类名
		    closeBtn: false, //不显示关闭按钮
		    shadeClose: true, //开启遮罩关闭
		    shade: false //不显示遮罩
		}, function(){
			var id = $('#id').val();
			$.ajax({
				url:baselocation+'/product/'+id,
				//contentType:'application/json',
				type:'delete',
				dataType:'json',
				//data: JSON.stringify(product),
				success:function(result){
					if(result.success==false){
						layer.msg(result.message,{icon:2,time:2000});
					}else{
						layer.msg(result.message,{icon:1,time:2000});
						setTimeout(goToProductList, 2000);
					}
				},
				error:function(result){
					alert("系统异常,请联系管理员！");
				}
			});
		}, function(){
		    //取消
		});
	})
	//跳转list页
	function goToProductList(){
		window.location.href=baselocation+'/product';
	}
});