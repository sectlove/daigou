package com.daigou.dto;

import java.util.List;

/**
 * 
 * @author lizhi
 *
 * @param <T>
 */
public class PageInfo<T> {
	
	private int total; // 总记录 
	
	private int pageIndex; //当前显示页
	
	private int pageSize = 10; //默认显示的记录数
	
	private int totalPage; //总页数
	
	private int dbIndex; //数据库中limit的参数，从第几条开始取
	
	private int dbSize = 10; //数据库中的limit的参数，一共取多少条
	
	private List<T> rows;

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
		this.count();
	}
	
	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
		this.count();
	}

	public int getDbSize() {
		return dbSize;
	}

	public void setDbSize(int dbSize) {
		this.dbSize = dbSize;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}
	
	public int getDbIndex() {
		return dbIndex;
	}

	public void setDbIndex(int dbIndex) {
		this.dbIndex = dbIndex;
	}


	public void count(){
		//计算总页数
		int totalPageTemp=this.total/this.pageSize;
		int plus=(this.total%this.pageSize)==0?0:1;
		totalPageTemp=totalPageTemp+plus;
		if(totalPageTemp<=0){
			totalPageTemp=1;
		}
		this.totalPage=totalPageTemp;
		//设置当前页数（总页数小于当前页数，应将当前页数设置为总页数）
		if(this.totalPage<this.pageIndex){
			this.totalPage=this.pageIndex;
		}
		//当前页数小于1设置为1
		if(this.pageIndex<1){
			this.pageIndex=1;
		}
		//设置limit的参数
		this.dbIndex = (this.pageIndex-1)*this.pageSize;
		this.dbSize = pageIndex * this.pageSize;
	}

	public List<T> getRows() {
		return rows;
	}

	public void setRows(List<T> rows) {
		this.rows = rows;
	}
}
