package com.daigou.dto;

import java.io.Serializable;

/**
 * 
 * @author lizhi
 *
 */
public class ProductQuery extends Query implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**状态 0正常 1冻结 2删除*/
    private Integer status;

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
