package com.daigou.dto;

import java.io.Serializable;
import java.util.List;

import com.daigou.model.Product;
/**
 * 商品信息包装类-给前端用的
 * @author lizhi
 *
 */
public class ProductDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//商品列表
	private List<Product> productList;
	//总商品数量
	private Integer totalCount = 0;
	//上架商品数量
	private Integer onSaleCount = 0;
	//下架商品数量
	private Integer offSaleCount = 0;
	//caogao商品数量
	private Integer caogaoCount = 0;
	public List<Product> getProductList() {
		return productList;
	}
	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}
	public Integer getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
	public Integer getOnSaleCount() {
		return onSaleCount;
	}
	public void setOnSaleCount(Integer onSaleCount) {
		this.onSaleCount = onSaleCount;
	}
	public Integer getOffSaleCount() {
		return offSaleCount;
	}
	public void setOffSaleCount(Integer offSaleCount) {
		this.offSaleCount = offSaleCount;
	}
	public Integer getCaogaoCount() {
		return caogaoCount;
	}
	public void setCaogaoCount(Integer caogaoCount) {
		this.caogaoCount = caogaoCount;
	}
}
