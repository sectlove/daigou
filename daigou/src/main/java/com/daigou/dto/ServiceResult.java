package com.daigou.dto;

/**  
 * @Title: ServiceResult.java
 * @Package com.daigou.dto;
 * @Description:类描述：返回结果类 
 * @author lizhi
 * @date 2016年12月6日
 */
public class ServiceResult {
	
	/** 返回结果 */
	private boolean success;
	/** 返回信息 */
	private String message;
	/** 返回信息 */
	private int errCode;
	/** 返回数据 */
	private Object data;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getErrCode() {
		return errCode;
	}

	public void setErrCode(int errCode) {
		this.errCode = errCode;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	public ServiceResult() {
	}
	public ServiceResult(boolean success) {
		super();
		this.success = success;
	}

	public ServiceResult(boolean success, String message) {
		super();
		this.success = success;
		this.message = message;
	}
	
	public ServiceResult(boolean success, Object data) {
		super();
		this.success = success;
		this.data = data;
	}

	public ServiceResult(boolean success, String message, Object data) {
		super();
		this.success = success;
		this.message = message;
		this.data = data;
	}
}
