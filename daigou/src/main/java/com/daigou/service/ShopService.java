package com.daigou.service;

import java.util.List;

import com.daigou.dto.ServiceResult;
import com.daigou.model.Shop;
import com.daigou.model.User;

 

/**  
 * @Title: ShopService.java
 * @Package com.daigou.service
 * @Description: TODO
 * @author lizhi
 * @date 2018年3月27日
 */
public interface ShopService{
	int deleteByPrimaryKey(Long id);

	ServiceResult insert(User user,Shop record);

    int insertSelective(Shop record);

    Shop selectByPrimaryKey(Long id);
    
    public Shop selectByCode(String code);
    
    int updateByPrimaryKeySelective(Shop record);

    int updateByPrimaryKey(Shop record);
    public List<Shop> selectAllShop();
}  