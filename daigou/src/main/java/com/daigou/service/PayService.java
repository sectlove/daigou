package com.daigou.service;

import java.math.BigDecimal;
import java.util.List;

import com.daigou.dto.ServiceResult;
import com.daigou.model.Pay;

 

/**  
 * @Title: PayService.java
 * @Package com.daigou.service
 * @Description: TODO
 * @author lizhi
 * @date 2018年3月27日
 */
public interface PayService{
	
	Pay selectByPrimaryKey(Long id);
	
	public int insert(Pay record);

	public ServiceResult insertPayAndUpdateOrder(Pay record) throws Exception;
	/**
	 * 为前端写的支付逻辑
	 * @param pay
	 * @return
	 * @throws Exception
	 */
	public ServiceResult insertPayAndUpdateOrder4Front(Pay pay) throws Exception;
	
	public int updateByPrimaryKeySelective(Pay record);
	
	public List<Pay> selectByOrderId(Long shopId,Long orderId);
	
	public BigDecimal selectPayAmountByOrderId(Long shopId,Long orderId);
}  