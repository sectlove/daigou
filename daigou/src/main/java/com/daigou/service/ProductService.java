package com.daigou.service;

import java.util.List;

import com.daigou.dto.ProductQuery;
import com.daigou.dto.ServiceResult;
import com.daigou.model.Product;

 

/**  
 * @Title: ProductService.java
 * @Package com.daigou.service
 * @Description: TODO
 * @author lizhi
 * @date 2018年3月27日
 */
public interface ProductService{
	public int deleteByPrimaryKey(Long id);

	public ServiceResult insertOrUpdate(Product product);
	
	public int insert(Product record);

	public int insertSelective(Product record);
	/**
	 * 购物车商品列表
	 * @param ids
	 * @return
	 */
	public List<Product> selectByIds(String ids);

	public Product selectByPrimaryKey(Long id);
	
	public List<Product> selectByShopId(Long shopId,Integer status,String searchKey);
	/**
	 * 分页查询
	 * @param productQuery
	 * @return
	 */
	public List<Product> selectByProductByPage(ProductQuery productQuery);
	/**
	 * 查询草稿状态的product
	 * @param shopId
	 * @param searchKey
	 * @return
	 */
	public List<Product> selectCaogaoByShopId(Long shopId,String searchKey);
	
	/**
     * 查总数（不分页）
     * @param shopId
     * @return
     */
    public Integer selectOnSaleCountByShopId(Long shopId);
	
	public Integer selectTotalCountByPage(ProductQuery productQuery);
	
	public Integer selectOnSaleCountByPage(ProductQuery productQuery);
	
	public Integer selectCaogaoCountByPage(ProductQuery productQuery);
	
	public Integer selectOffSaleCountByPage(ProductQuery productQuery);

	public int updateByPrimaryKeySelective(Product record);

	public int updateByPrimaryKeyWithBLOBs(Product record);

	public int updateByPrimaryKey(Product record);
}  