package com.daigou.service;

import com.daigou.dto.CustomerQuery;
import com.daigou.dto.PageInfo;
import com.daigou.dto.ServiceResult;
import com.daigou.model.Customer;
import com.daigou.model.Shop;
import com.daigou.model.ShopCustomer;

public interface CustomerService {
	
	public ShopCustomer selectByPrimaryKey(Long id);
	/**
	 * left join 联表查询
	 */
	public ShopCustomer selectByShopIdAndMobile(Long shopId,Long mobile);
	
	public int deleteByPrimaryKey(Long id);
	
	public ServiceResult insertOrUpdate(ShopCustomer shopCustomer);
	/**
	 * 保存商铺客户信息
	 * @param shop
	 * @param customer
	 * @return
	 */
	public int saveCustomer(Shop shop,Customer customer);
	
	public int updateCustomer(Shop shop,Customer customer);
	
	public PageInfo<ShopCustomer> selectShopCustomer(CustomerQuery customerQuery);
	
	public Customer selectByMobile(Long mobile);
}
