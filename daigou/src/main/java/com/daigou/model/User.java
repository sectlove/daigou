package com.daigou.model;

import java.io.Serializable;
import java.util.Date;

import com.daigou.tools.Md5;

public class User implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;//UUID
	private Long shopId;//店铺id
    private String loginName;//登录名
    private String name;
    private String pwd = Md5.MD5("123456");//密码,默认6个1
    private String repwd;//密码
    //0,超级管理员,1，店铺管理员，2，业务经理，3，业务员
    private Integer type=3;
    //状态,0,冻结，10，待完善user，11，待完善shop,20,ok
    private int status;
    private String email;//邮箱
    private String mobile;//电话
    private String weixin;
    private String zhifubao;
    private String qq;
    private String weixinQrCode;
    private String zhifubaoQrCode;
    private String qqQrCode;
    private String idCard;//身份证
    private Integer sex = -1;//性别
    private String address;//地址  
    private Long leaderId;//负责人id
    private String leaderName;//负责人名称
    private Long creatorId;//创建人
    private Date createdTime;//创建时间=注册时间
    private Long modifierId;//修改人
    private Date modifiedTime;//修改时间
    //-1：删除，0，草稿，1，正常
    private int dr=1;//删除标记
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getRepwd() {
		return repwd;
	}
	public void setRepwd(String repwd) {
		this.repwd = repwd;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getWeixin() {
		return weixin;
	}
	public void setWeixin(String weixin) {
		this.weixin = weixin;
	}
	public String getZhifubao() {
		return zhifubao;
	}
	public void setZhifubao(String zhifubao) {
		this.zhifubao = zhifubao;
	}
	public String getQq() {
		return qq;
	}
	public void setQq(String qq) {
		this.qq = qq;
	}
	public String getWeixinQrCode() {
		return weixinQrCode;
	}
	public void setWeixinQrCode(String weixinQrCode) {
		this.weixinQrCode = weixinQrCode;
	}
	public String getZhifubaoQrCode() {
		return zhifubaoQrCode;
	}
	public void setZhifubaoQrCode(String zhifubaoQrCode) {
		this.zhifubaoQrCode = zhifubaoQrCode;
	}
	public String getQqQrCode() {
		return qqQrCode;
	}
	public void setQqQrCode(String qqQrCode) {
		this.qqQrCode = qqQrCode;
	}
	public String getIdCard() {
		return idCard;
	}
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	public Integer getSex() {
		return sex;
	}
	public void setSex(Integer sex) {
		this.sex = sex;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Long getLeaderId() {
		return leaderId;
	}
	public void setLeaderId(Long leaderId) {
		this.leaderId = leaderId;
	}
	
	public String getLeaderName() {
		return leaderName;
	}
	public void setLeaderName(String leaderName) {
		this.leaderName = leaderName;
	}
	public Long getCreatorId() {
		return creatorId;
	}
	public void setCreatorId(Long creatorId) {
		this.creatorId = creatorId;
	}
	public Long getModifierId() {
		return modifierId;
	}
	public void setModifierId(Long modifierId) {
		this.modifierId = modifierId;
	}
	public Date getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	public Date getModifiedTime() {
		return modifiedTime;
	}
	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	public int getDr() {
		return dr;
	}
	public void setDr(int dr) {
		this.dr = dr;
	}
}