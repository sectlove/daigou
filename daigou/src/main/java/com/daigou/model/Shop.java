package com.daigou.model;

import java.io.Serializable;
import java.util.Date;

public class Shop implements Serializable {
    private Long id;

    private Long salerId;

    private String code;

    private String name;

    private String logo;

    private Integer sortNo;

    private Integer status;
  //地址信息
    private String province;
    
    private String city;
    
    private String address;

    private Date startTime;

    private Date endTime;

    private String description;

    private String zhifubaoQrCode;

    private String weixinQrCode;

    private String zhifubaoQrCode2;

    private String weixinQrCode2;

    private Long creatorId;

    private Date createdTime;

    private Long modifierId;

    private Date modifiedTime;
    
    private Integer onSaleCount = 0;
    //-1：删除，0，草稿，1，正常
    private Integer dr=1;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSalerId() {
        return salerId;
    }

    public void setSalerId(Long salerId) {
        this.salerId = salerId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo == null ? null : logo.trim();
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getZhifubaoQrCode() {
        return zhifubaoQrCode;
    }

    public void setZhifubaoQrCode(String zhifubaoQrCode) {
        this.zhifubaoQrCode = zhifubaoQrCode == null ? null : zhifubaoQrCode.trim();
    }

    public String getWeixinQrCode() {
        return weixinQrCode;
    }

    public void setWeixinQrCode(String weixinQrCode) {
        this.weixinQrCode = weixinQrCode == null ? null : weixinQrCode.trim();
    }

    public String getZhifubaoQrCode2() {
        return zhifubaoQrCode2;
    }

    public void setZhifubaoQrCode2(String zhifubaoQrCode2) {
        this.zhifubaoQrCode2 = zhifubaoQrCode2 == null ? null : zhifubaoQrCode2.trim();
    }

    public String getWeixinQrCode2() {
        return weixinQrCode2;
    }

    public void setWeixinQrCode2(String weixinQrCode2) {
        this.weixinQrCode2 = weixinQrCode2 == null ? null : weixinQrCode2.trim();
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Long getModifierId() {
        return modifierId;
    }

    public void setModifierId(Long modifierId) {
        this.modifierId = modifierId;
    }

    public Date getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public Integer getOnSaleCount() {
		return onSaleCount;
	}

	public void setOnSaleCount(Integer onSaleCount) {
		this.onSaleCount = onSaleCount;
	}

	public Integer getDr() {
        return dr;
    }

    public void setDr(Integer dr) {
        this.dr = dr;
    }

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}