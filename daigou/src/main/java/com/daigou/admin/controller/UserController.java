package com.daigou.admin.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.daigou.constants.CommonConstants;
import com.daigou.dto.AjaxResult;
import com.daigou.dto.ServiceResult;
import com.daigou.dto.UserQuery;
import com.daigou.model.Shop;
import com.daigou.model.User;
import com.daigou.service.CustomerService;
import com.daigou.service.UserService;

/**  
 * @Title: UserController.java
 * @Description: TODO
 * @author lizhi
 * @date 2018年3月16日
 */
@Controller("adminUserController")
@RequestMapping("/admin")
public class UserController{
	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private CustomerService customerService;
	
	@RequestMapping(value = "/account",method = RequestMethod.GET)
    public String toAccountPage(HttpServletRequest request,Model model){
		LOGGER.info("进入我的账户页面");
		Shop shop = (Shop)request.getAttribute(CommonConstants.SHOP_INFO);
		model.addAttribute("shop", shop);
		User user = (User)request.getAttribute(CommonConstants.USER_INFO);
		model.addAttribute("user", user);
        return "/admin/app/account";
    }
	@RequestMapping(value = "/user-info",method = RequestMethod.GET)
    public String toUserInfoPage(HttpServletRequest request,Model model){
		LOGGER.info("进入我的个人信息页面");
		User user = (User)request.getAttribute(CommonConstants.USER_INFO);
		model.addAttribute("user", user);
        return "/admin/app/user_info";
    }
	@RequestMapping(value = "/user-update",method = RequestMethod.GET)
    public String toUserUpdateoPage(HttpServletRequest request,Model model){
		LOGGER.info("进入我的个人信息页面");
		User user = (User)request.getAttribute(CommonConstants.USER_INFO);
		model.addAttribute("user", user);
        return "/admin/app/user_update";
    }
	@RequestMapping(value = "/user-update",method = RequestMethod.POST)
	@ResponseBody
    public AjaxResult updateDoneUser(HttpServletRequest request,@RequestBody User user){
		LOGGER.info("进入完善用户信息页面");
		User userInfo = (User)request.getAttribute(CommonConstants.USER_INFO);
		userInfo.setName(user.getName());
		userInfo.setWeixin(user.getWeixin());
		userInfo.setWeixinQrCode(user.getWeixinQrCode());
		userInfo.setZhifubao(user.getZhifubao());
		userInfo.setZhifubaoQrCode(user.getZhifubaoQrCode());
		userInfo.setQq(user.getQq());
		userInfo.setSex(user.getSex());
		userInfo.setIdCard(user.getIdCard());
		userInfo.setAddress(user.getAddress());
		//密码不能从这里改
		userInfo.setPwd(null);
		userService.updateByPrimaryKeySelective(userInfo);
        return new AjaxResult(true,"个人信息更新成功!");
    }
	@RequestMapping(value = "/employee",method = RequestMethod.GET)
    public String toEmployeePage(HttpServletRequest request,Model model){
		LOGGER.info("进入员工管理页面");
		User user = (User)request.getAttribute(CommonConstants.USER_INFO);
		UserQuery userQuery = new UserQuery();
		userQuery.setShopId(user.getShopId());
		List<User> employeeList = userService.selectMyEmployee(userQuery);
		model.addAttribute("employeeList", employeeList);
        return "/admin/app/employee_list";
    }
	/**
	 * 新增和修改
	 * @return
	 */
	@RequestMapping(value = "/employee",method = RequestMethod.POST)
	@ResponseBody
    public AjaxResult employee(HttpServletRequest request,@RequestBody User employee){
		User userInfo = (User)request.getAttribute(CommonConstants.USER_INFO);
		employee.setShopId(userInfo.getShopId());
		employee.setCreatorId(userInfo.getId());
		employee.setCreatedTime(new Date());
		employee.setModifierId(userInfo.getId());
		employee.setCreatedTime(new Date());
		ServiceResult serviceResult = userService.insertOrUpdateEmployee(employee);
        return new AjaxResult(serviceResult);
    }
	/**
	 * 新增和编辑页
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/employee/{id}",method = RequestMethod.GET)
    public String toEmployeePage(HttpServletRequest request,Model model,@PathVariable("id")Long id){
		User employee = null;
		User user = (User)request.getAttribute(CommonConstants.USER_INFO);
		model.addAttribute("user", user);
		UserQuery userQuery = new UserQuery();
		userQuery.setShopId(user.getShopId());
		userQuery.setType("2");
		List<User> employeeList = userService.selectMyEmployee(userQuery);
		model.addAttribute("employeeList", employeeList);
		if(id == null || id.equals(0L)){
			LOGGER.info("进入员工新增{}",id);
			employee = new User();
			employee.setId(id);
			employee.setDr(0);
			model.addAttribute("employeeTitle", "员工新增");
		}else{
			LOGGER.info("进入员工更新{}",id);
			employee = userService.selectByPrimaryKey(id);
			model.addAttribute("employeeTitle", "员工编辑");
		}
		model.addAttribute("employee", employee);
        return "/admin/app/employee_update";
    }
	@RequestMapping(value = "/employee/{id}/details",method = RequestMethod.GET)
    public String toEmployeeDetailsPage(Model model,@PathVariable("id")Long id){
		LOGGER.info("进入员工详情{}",id);
		User employee = userService.selectByPrimaryKey(id);
		model.addAttribute("employee", employee);
        return "/admin/app/employee_details";
    }
	@RequestMapping(value = "/employee-query",method = RequestMethod.GET)
	@ResponseBody
    public AjaxResult queryEmployeeList(HttpServletRequest request,@RequestParam("searchKey")String searchKey){
		LOGGER.info("查询员工");
		User user = (User)request.getAttribute(CommonConstants.USER_INFO);
		UserQuery userQuery = new UserQuery();
		userQuery.setShopId(user.getShopId());
		userQuery.setSearchKey(searchKey);
		List<User> employeeList = userService.selectMyEmployee(userQuery);
		return new AjaxResult(true,employeeList);
    }
	@RequestMapping(value = "/proposal",method = RequestMethod.GET)
    public String toProposalPage(){
		LOGGER.info("进入我的评价页面");
        return "/admin/app/proposal";
    }
	@RequestMapping(value = "/help",method = RequestMethod.GET)
    public String toHelpPage(){
		LOGGER.info("进入帮助页面");
        return "/admin/app/help";
    }
	@RequestMapping(value = "/password",method = RequestMethod.GET)
    public String toPasswordPage(){
		LOGGER.info("进入修改密码页面");
        return "/admin/app/password";
    }
	
	@RequestMapping(value = "/password",method = RequestMethod.POST)
	@ResponseBody
    public AjaxResult updatePassword(HttpServletRequest request,@RequestBody User user){
		LOGGER.info("修改密码");
		if(user.getPwd() == null || "".equals(user.getPwd())){
			return new AjaxResult(false,"请输入旧密码!");
		}
		if(user.getRepwd() == null || "".equals(user.getRepwd())){
			return new AjaxResult(false,"请输入新密码!");
		}
		User userInfo = (User)request.getAttribute(CommonConstants.USER_INFO);
		User userDb = userService.login(userInfo.getLoginName(), user.getPwd());
		if(userDb == null){
			return new AjaxResult(false,"旧密码错误!");
		}
		userService.updatePasswordById(userInfo.getId(), user.getRepwd());
        return new AjaxResult(true,"密码修改成功,请重新登录!");
    }
}
