package com.daigou.admin.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.daigou.constants.CommonConstants;
import com.daigou.dto.AjaxResult;
import com.daigou.dto.ProductDto;
import com.daigou.dto.ProductQuery;
import com.daigou.dto.ServiceResult;
import com.daigou.model.Product;
import com.daigou.model.Shop;
import com.daigou.model.User;
import com.daigou.service.ProductService;

@Controller("adminProductController")
@RequestMapping(value = "/admin")
public class ProductController {
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);
	@Autowired
	private ProductService productService;
	
	/**
	 * 查看详情和编辑页
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/product/{id}",method = RequestMethod.GET)
    public String toProductPage(HttpServletRequest request,Model model,@PathVariable("id")Long id){
		Product product = null;
		User user = (User)request.getAttribute(CommonConstants.USER_INFO);
		model.addAttribute("user", user);
		if(id == null || id.equals(0L)){
			LOGGER.info("进入商品新增{}",id);
			product = new Product();
			product.setId(id);
			product.setDr(0);
			model.addAttribute("productTitle", "商品新增");
		}else{
			LOGGER.info("进入商品更新{}",id);
			product = productService.selectByPrimaryKey(id);
			model.addAttribute("productTitle", "商品编辑");
		}
		model.addAttribute("product", product);
        return "/admin/app/product_update";
    }
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/product/{id}",method = RequestMethod.DELETE)
	@ResponseBody
    public AjaxResult deleteProduct(HttpServletRequest request,Model model,@PathVariable("id")Long id){
		if(id == null || id.equals(0L)){
			LOGGER.info("进入商品删除{}",id);
			return new AjaxResult(false,"删除失败,请传入商品id!");
		}else{
			LOGGER.info("进入商品更新{}",id);
			int result = productService.deleteByPrimaryKey(id);
			if(result == 0){
				return new AjaxResult(false,"商品不存在,请刷新后重试!");
			}
			return new AjaxResult(true,"商品删除成功!");
		}
    }
	@RequestMapping(value = "/product/{id}/details",method = RequestMethod.GET)
    public String toProductDetailsPage(HttpServletRequest request,Model model,@PathVariable("id")Long id){
		LOGGER.info("进入商品详情{}",id);
		User user = (User)request.getAttribute(CommonConstants.USER_INFO);
		model.addAttribute("user", user);
		Product product = productService.selectByPrimaryKey(id);
		model.addAttribute("product", product);
        return "/admin/app/product_details";
    }
	/**
	 * 新增和修改
	 * @return
	 */
	@RequestMapping(value = "/product",method = RequestMethod.POST)
	@ResponseBody
    public AjaxResult product(HttpServletRequest request,@RequestBody Product product){
		User user = (User)request.getAttribute(CommonConstants.USER_INFO);
		product.setShopId(user.getShopId());
		product.setCreatorId(user.getId());
		product.setCreatedTime(new Date());
		product.setModifierId(user.getId());
		product.setModifiedTime(new Date());
		ServiceResult serviceResult = productService.insertOrUpdate(product);
        return new AjaxResult(serviceResult);
    }
	
	/**
	 * 新增和修改
	 * @return
	 */
	@RequestMapping(value = "/product-update-status",method = RequestMethod.POST)
	@ResponseBody
    public AjaxResult productUpdateStatus(HttpServletRequest request,@RequestBody Product product){
		User user = (User)request.getAttribute(CommonConstants.USER_INFO);
		product.setModifierId(user.getId());
		product.setModifiedTime(new Date());
		product.setSalePrice(null);
		product.setCostPrice(null);
		product.setMarketPrice(null);
		product.setSortNo(null);
		product.setDr(null);
		int result = productService.updateByPrimaryKeySelective(product);
		if(result == 0){
			return new AjaxResult(false,"状态更新失败,请刷新后重试！");
		}
        return new AjaxResult(true,"状态更新成功！");
    }
	@RequestMapping(value = "/product",method = RequestMethod.GET)
    public String toProductListPage(HttpServletRequest request,Model model){
		LOGGER.info("进入商品页面");
		ProductQuery productQuery = new ProductQuery();
		User user = (User)request.getAttribute(CommonConstants.USER_INFO);
		model.addAttribute("user", user);
		productQuery.setShopId(user.getShopId());
		productQuery.setUser(user);
		//设置分页
		productQuery.buildPage();
		List<Product> productList = productService.selectByProductByPage(productQuery);
		model.addAttribute("productList", productList);
		Integer onSaleCount = productService.selectOnSaleCountByPage(productQuery);
		model.addAttribute("onSaleCount", onSaleCount);
		Integer offSaleCount = productService.selectOffSaleCountByPage(productQuery);
		model.addAttribute("offSaleCount", offSaleCount);
		Integer totalCount = productService.selectTotalCountByPage(productQuery);
		model.addAttribute("totalCount", totalCount);
		Integer caogaoCount = productService.selectCaogaoCountByPage(productQuery);
		model.addAttribute("caogaoCount", caogaoCount);
        return "/admin/app/product_list";
    }
	
	@RequestMapping(value = "/product-caogao",method = RequestMethod.GET)
    public String toProductCaogaoListPage(HttpServletRequest request,Model model){
		LOGGER.info("进入商品页面");
		Shop shop = (Shop)request.getAttribute(CommonConstants.SHOP_INFO);
		List<Product> productList = productService.selectCaogaoByShopId(shop.getId(),null);
		model.addAttribute("productList", productList);
        return "/admin/app/product_caogao";
    }
	
	@RequestMapping(value = "/order-product-list",method = RequestMethod.GET)
    public String toOrderProductListPage(HttpServletRequest request,Model model){
		LOGGER.info("从订单管理进入商品页面");
		ProductQuery productQuery = new ProductQuery();
		User user = (User)request.getAttribute(CommonConstants.USER_INFO);
		productQuery.setShopId(user.getShopId());
		productQuery.setUser(user);
		productQuery.setStatus(1);
		//设置分页
		productQuery.buildPage();
		//此处只能找到在售的商品
		List<Product> productList = productService.selectByProductByPage(productQuery);
		model.addAttribute("productList", productList);
		Integer onSaleCount = productService.selectOnSaleCountByPage(productQuery);
		model.addAttribute("onSaleCount", onSaleCount);
		Integer offSaleCount = productService.selectOffSaleCountByPage(productQuery);
		model.addAttribute("offSaleCount", offSaleCount);
		Integer totalCount = productService.selectTotalCountByPage(productQuery);
		model.addAttribute("totalCount", totalCount);
		Integer caogaoCount = productService.selectCaogaoCountByPage(productQuery);
		model.addAttribute("caogaoCount", caogaoCount);
        return "/admin/app/order_product_list";
    }
	
	@RequestMapping(value = "/product-query",method = RequestMethod.POST)
	@ResponseBody
    public AjaxResult queryProductList(HttpServletRequest request,@RequestBody ProductQuery productQuery){
		LOGGER.info("查询商品");
		User user = (User)request.getAttribute(CommonConstants.USER_INFO);
		productQuery.setShopId(user.getShopId());
		productQuery.setUser(user);
		//设置分页
		productQuery.buildPage();
		List<Product> productList = productService.selectByProductByPage(productQuery);
		ProductDto productDto = new ProductDto();
		productDto.setProductList(productList);
		Integer onSaleCount = productService.selectOnSaleCountByPage(productQuery);
		productDto.setOnSaleCount(onSaleCount);
		Integer offSaleCount = productService.selectOffSaleCountByPage(productQuery);
		productDto.setOffSaleCount(offSaleCount);
		Integer totalCount = productService.selectTotalCountByPage(productQuery);
		productDto.setTotalCount(totalCount);
		Integer caogaoCount = productService.selectCaogaoCountByPage(productQuery);
		productDto.setCaogaoCount(caogaoCount);
		return new AjaxResult(true,productDto);
    }
}
