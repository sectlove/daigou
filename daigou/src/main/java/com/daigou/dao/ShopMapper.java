package com.daigou.dao;

import java.util.List;

import com.daigou.model.Shop;

public interface ShopMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Shop record);

    int insertSelective(Shop record);

    Shop selectByPrimaryKey(Long id);
    
    Shop selectByCode(String code);
    
    int updateByPrimaryKeySelective(Shop record);

    int updateByPrimaryKey(Shop record);
    
    List<Shop> selectAllShop();
}