package com.daigou.dao;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.daigou.model.Pay;

public interface PayMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Pay record);

    int insertSelective(Pay record);

    Pay selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Pay record);

    int updateByPrimaryKey(Pay record);
    
    List<Pay> selectByOrderId(@Param("shopId")Long shopId,@Param("orderId")Long orderId);
    
    public BigDecimal selectPayAmountByOrderId(@Param("shopId")Long shopId,@Param("orderId")Long orderId);
}