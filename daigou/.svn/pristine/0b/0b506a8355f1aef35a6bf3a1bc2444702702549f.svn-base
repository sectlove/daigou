package com.daigou.admin.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.daigou.constants.CommonConstants;
import com.daigou.dto.AjaxResult;
import com.daigou.model.Shop;
import com.daigou.model.Trip;
import com.daigou.model.User;
import com.daigou.service.TripService;

@Controller("adminTripController")
@RequestMapping(value = "/admin")
public class TripController {
	private static final Logger LOGGER = LoggerFactory.getLogger(TripController.class);
	@Autowired
	private TripService tripService;
	@RequestMapping(value = "/go-go")
    public String toGoGoPage(HttpServletRequest request,Model model){
		LOGGER.info("进入行程列表页面");
		Shop shop = (Shop)request.getAttribute(CommonConstants.SHOP_INFO);
		List<Trip> tripList = tripService.selectByShopId(shop.getId());
		model.addAttribute("tripList", tripList);
		return "/admin/app/trip";
    }
	@RequestMapping(value = "/trip/{id}",method = RequestMethod.GET)
    public String toTripUpdatePage(HttpServletRequest request,Model model,@PathVariable("id")Long id){
		LOGGER.info("进入行程编辑或者新增页面");
		Trip trip = null;
		User user = (User)request.getAttribute(CommonConstants.USER_INFO);
		model.addAttribute("user", user);
		if(id == null || id.equals(0L)){
			LOGGER.info("进入行程新增{}",id);
			trip = new Trip();
			trip.setId(id);
			model.addAttribute("tripTitle", "行程新增");
		}else{
			LOGGER.info("进入行程更新{}",id);
			trip = tripService.selectByPrimaryKey(id);
			model.addAttribute("trip", trip);
			model.addAttribute("tripTitle", "行程编辑");
		}
        return "/admin/app/trip_update";
    }
	@RequestMapping(value = "/trip",method = RequestMethod.POST)
	@ResponseBody
    public AjaxResult tripSave(HttpServletRequest request,@RequestBody Trip trip){
		Shop shop = (Shop)request.getAttribute(CommonConstants.SHOP_INFO);
		trip.setSalerId(shop.getSalerId());
		trip.setShopId(shop.getId());
		trip.setShopName(shop.getName());
		trip.setCreatorId(shop.getSalerId());
		trip.setCreatedTime(new Date());
		tripService.insert(trip);
        return new AjaxResult(true,"行程创建成功!");
    }
}
