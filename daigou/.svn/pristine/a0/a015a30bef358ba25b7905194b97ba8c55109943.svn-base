<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.daigou.dao.OrderMapper">
  <resultMap id="BaseResultMap" type="com.daigou.model.Order">
    <id column="id" jdbcType="BIGINT" property="id" />
    <result column="saler_id" jdbcType="BIGINT" property="salerId" />
    <result column="shop_id" jdbcType="BIGINT" property="shopId" />
    <result column="shop_name" jdbcType="VARCHAR" property="shopName" />
    <result column="product_id" jdbcType="BIGINT" property="productId" />
    <result column="product_name" jdbcType="VARCHAR" property="productName" />
    <result column="price" jdbcType="DECIMAL" property="price" />
    <result column="num" jdbcType="DECIMAL" property="num" />
    <result column="cate" jdbcType="VARCHAR" property="cate" />
    <result column="spec" jdbcType="VARCHAR" property="spec" />
    <result column="brand" jdbcType="VARCHAR" property="brand" />
    <result column="production_place" jdbcType="VARCHAR" property="productionPlace" />
    <result column="buyer_code" jdbcType="VARCHAR" property="buyerCode" />
    <result column="buyer_name" jdbcType="VARCHAR" property="buyerName" />
    <result column="address" jdbcType="VARCHAR" property="address" />
    <result column="pic_main" jdbcType="VARCHAR" property="picMain" />
    <result column="remark" jdbcType="VARCHAR" property="remark" />
    <result column="status" jdbcType="INTEGER" property="status" />
    <result column="pay_status" jdbcType="INTEGER" property="payStatus" />
    <result column="send_no" jdbcType="VARCHAR" property="sendNo" />
    <result column="send_company" jdbcType="VARCHAR" property="sendCompany" />
    <result column="send_time" jdbcType="TIMESTAMP" property="sendTime" />
    <result column="total_amount" jdbcType="DECIMAL" property="totalAmount" />
    <result column="pay_amount" jdbcType="DECIMAL" property="payAmount" />
    <result column="no_pay_amount" jdbcType="DECIMAL" property="noPayAmount" />
    <result column="creator_id" jdbcType="BIGINT" property="creatorId" />
    <result column="created_time" jdbcType="TIMESTAMP" property="createdTime" />
    <result column="modifier_id" jdbcType="BIGINT" property="modifierId" />
    <result column="modified_time" jdbcType="TIMESTAMP" property="modifiedTime" />
    <result column="dr" jdbcType="INTEGER" property="dr" />
  </resultMap>
  <sql id="Base_Column_List">
    id, saler_id, shop_id, shop_name,product_id, product_name, price, num, cate, spec, brand,production_place, buyer_code, 
    buyer_name, address, pic_main, remark, status,pay_status, send_no, send_company, send_time, 
    total_amount, pay_amount, no_pay_amount, creator_id, created_time, modifier_id, modified_time, 
    dr
  </sql>
  <select id="selectByPrimaryKey" parameterType="java.lang.Long" resultMap="BaseResultMap">
    select 
    <include refid="Base_Column_List" />
    from tb_order
    where id = #{id,jdbcType=BIGINT}
  </select>
  <select id="selectCaogaoByShopId" resultMap="BaseResultMap" parameterType="java.util.Map">
    select 
    <include refid="Base_Column_List" />
    from tb_order
    where shop_id = #{shopId,jdbcType=BIGINT}
    and dr = 0
    <if test="searchKey != null and searchKey !=''">
    and (
	   	product_name like concat(#{searchKey,jdbcType=VARCHAR},'%')
	   	or
	    spec like concat(#{searchKey,jdbcType=VARCHAR},'%')
	    or
	    cate like concat(#{searchKey,jdbcType=VARCHAR},'%')
	    or
	    brand like concat(#{searchKey,jdbcType=VARCHAR},'%')
	    or
	    production_place like concat(#{searchKey,jdbcType=VARCHAR},'%')
    )
    </if>
    order by created_time desc,status desc
  </select>
  <select id="selectTotalCaogaoByShopId" resultType="java.lang.Integer" parameterType="java.util.Map">
    select 
    count(1)
    from tb_order
    where shop_id = #{shopId,jdbcType=BIGINT}
    and dr = 0
    <if test="searchKey != null and searchKey !=''">
    and (
	   	product_name like concat(#{searchKey,jdbcType=VARCHAR},'%')
	   	or
	    spec like concat(#{searchKey,jdbcType=VARCHAR},'%')
	    or
	    cate like concat(#{searchKey,jdbcType=VARCHAR},'%')
	    or
	    brand like concat(#{searchKey,jdbcType=VARCHAR},'%')
	    or
	    production_place like concat(#{searchKey,jdbcType=VARCHAR},'%')
    )
    </if>
    order by created_time desc,status desc
  </select>
  <sql id="queryCondition">
  	<if test="status != null and status =='678'">
  		and status in(1,2,3,4,5,6,7,8)
  	</if>
  	<if test="status != null and status !='678'">
  		and status = #{status,jdbcType=INTEGER}
  	</if>
  	<if test="filterTime != null and filterTime !=''">
  		and created_time >= #{filterTime,jdbcType=TIMESTAMP}
  	</if>
    <if test="searchKey != null and searchKey !=''">
	    and (
	    	id like concat(#{searchKey,jdbcType=VARCHAR},'%')
	    	or
		   	product_name like concat(#{searchKey,jdbcType=VARCHAR},'%')
		   	or
		    spec like concat(#{searchKey,jdbcType=VARCHAR},'%')
		    or
		    cate like concat(#{searchKey,jdbcType=VARCHAR},'%')
		    or
		    brand like concat(#{searchKey,jdbcType=VARCHAR},'%')
		    or
		    production_place like concat(#{searchKey,jdbcType=VARCHAR},'%')
	    )
    </if>
  </sql>
  <select id="selectByCondition" resultMap="BaseResultMap">
    select 
    <include refid="Base_Column_List" />
    from tb_order
    where shop_id = #{shopId,jdbcType=BIGINT}
    and dr = 1
    <include refid="queryCondition" />
    order by created_time desc,status desc
  </select>
  <select id="selectTotalCountByCondition" resultType="java.lang.Integer">
    select count(1) from tb_order 
    where 
    shop_id = #{shopId,jdbcType=BIGINT} 
    and dr = 1
    <include refid="queryCondition" />
  </select>
  <select id="selectTotalAmountByCondition" resultType="java.math.BigDecimal">
    select sum(total_amount) from tb_order 
    where 
    shop_id = #{shopId,jdbcType=BIGINT} 
    and dr = 1
    <include refid="queryCondition" />
  </select>
  <select id="selectPayAmountByCondition" resultType="java.math.BigDecimal">
    select sum(pay_amount) from tb_order 
    where 
    shop_id = #{shopId,jdbcType=BIGINT} 
    and dr = 1
    <include refid="queryCondition" />
  </select>
  <select id="selectNoPayAmountByCondition" resultType="java.math.BigDecimal">
    select sum(no_pay_amount) from tb_order 
    where 
    shop_id = #{shopId,jdbcType=BIGINT} 
    and dr = 1
    <include refid="queryCondition" />
  </select>
  <delete id="deleteByPrimaryKey" parameterType="java.lang.Long">
    delete from tb_order
    where id = #{id,jdbcType=BIGINT}
  </delete>
  <insert id="insert" parameterType="com.daigou.model.Order">
    insert into tb_order (id, saler_id, shop_id, 
      shop_name,product_id, product_name, price, 
      num, cate, spec, brand, production_place,
      buyer_code, buyer_name, address, 
      pic_main, remark, status,pay_status, 
      send_no, send_company, send_time, 
      total_amount, pay_amount, no_pay_amount, 
      creator_id, created_time, modifier_id, 
      modified_time, dr)
    values (#{id,jdbcType=BIGINT}, #{salerId,jdbcType=BIGINT}, #{shopId,jdbcType=BIGINT}, 
      #{shopName,jdbcType=VARCHAR},#{productId,jdbcType=BIGINT}, #{productName,jdbcType=VARCHAR}, #{price,jdbcType=DECIMAL}, 
      #{num,jdbcType=DECIMAL}, #{cate,jdbcType=VARCHAR}, #{spec,jdbcType=VARCHAR}, #{brand,jdbcType=VARCHAR},
      #{productionPlace,jdbcType=VARCHAR}, 
      #{buyerCode,jdbcType=VARCHAR}, #{buyerName,jdbcType=VARCHAR}, #{address,jdbcType=VARCHAR}, 
      #{picMain,jdbcType=VARCHAR}, #{remark,jdbcType=VARCHAR}, 
      #{status,jdbcType=INTEGER}, #{payStatus,jdbcType=INTEGER},
      #{sendNo,jdbcType=VARCHAR}, #{sendCompany,jdbcType=VARCHAR}, #{sendTime,jdbcType=TIMESTAMP}, 
      #{totalAmount,jdbcType=DECIMAL}, #{payAmount,jdbcType=DECIMAL}, #{noPayAmount,jdbcType=DECIMAL}, 
      #{creatorId,jdbcType=BIGINT}, #{createdTime,jdbcType=TIMESTAMP}, #{modifierId,jdbcType=BIGINT}, 
      #{modifiedTime,jdbcType=TIMESTAMP}, #{dr,jdbcType=INTEGER})
  </insert>
  <insert id="insertSelective" parameterType="com.daigou.model.Order">
    insert into tb_order
    <trim prefix="(" suffix=")" suffixOverrides=",">
      <if test="id != null">
        id,
      </if>
      <if test="salerId != null">
        saler_id,
      </if>
      <if test="shopId != null">
        shop_id,
      </if>
      <if test="shopName != null">
        shop_name,
      </if>
      <if test="productId != null">
        product_id,
      </if>
      <if test="productName != null">
        product_name,
      </if>
      <if test="price != null">
        price,
      </if>
      <if test="num != null">
        num,
      </if>
      <if test="cate != null">
        cate,
      </if>
      <if test="spec != null">
        spec,
      </if>
      <if test="brand != null">
        brand,
      </if>
      <if test="productionPlace != null">
        production_place,
      </if>
      <if test="buyerCode != null">
        buyer_code,
      </if>
      <if test="buyerName != null">
        buyer_name,
      </if>
      <if test="address != null">
        address,
      </if>
      <if test="picMain != null">
        pic_main,
      </if>
      <if test="remark != null">
        remark,
      </if>
      <if test="status != null">
        status,
      </if>
      <if test="payStatus != null">
        pay_status,
      </if>
      <if test="sendNo != null">
        send_no,
      </if>
      <if test="sendCompany != null">
        send_company,
      </if>
      <if test="sendTime != null">
        send_time,
      </if>
      <if test="totalAmount != null">
        total_amount,
      </if>
      <if test="payAmount != null">
        pay_amount,
      </if>
      <if test="noPayAmount != null">
        no_pay_amount,
      </if>
      <if test="creatorId != null">
        creator_id,
      </if>
      <if test="createdTime != null">
        created_time,
      </if>
      <if test="modifierId != null">
        modifier_id,
      </if>
      <if test="modifiedTime != null">
        modified_time,
      </if>
      <if test="dr != null">
        dr,
      </if>
    </trim>
    <trim prefix="values (" suffix=")" suffixOverrides=",">
      <if test="id != null">
        #{id,jdbcType=BIGINT},
      </if>
      <if test="salerId != null">
        #{salerId,jdbcType=BIGINT},
      </if>
      <if test="shopId != null">
        #{shopId,jdbcType=BIGINT},
      </if>
      <if test="shopName != null">
        #{shopName,jdbcType=VARCHAR},
      </if>
      <if test="productId != null">
        #{productId,jdbcType=BIGINT},
      </if>
      <if test="productName != null">
        #{productName,jdbcType=VARCHAR},
      </if>
      <if test="price != null">
        #{price,jdbcType=DECIMAL},
      </if>
      <if test="num != null">
        #{num,jdbcType=DECIMAL},
      </if>
      <if test="cate != null">
        #{cate,jdbcType=VARCHAR},
      </if>
      <if test="spec != null">
        #{spec,jdbcType=VARCHAR},
      </if>
      <if test="brand != null">
        #{brand,jdbcType=VARCHAR},
      </if>
      <if test="productionPlace != null">
        #{productionPlace,jdbcType=VARCHAR},
      </if>
      <if test="buyerCode != null">
        #{buyerCode,jdbcType=VARCHAR},
      </if>
      <if test="buyerName != null">
        #{buyerName,jdbcType=VARCHAR},
      </if>
      <if test="address != null">
        #{address,jdbcType=VARCHAR},
      </if>
      <if test="picMain != null">
        #{picMain,jdbcType=VARCHAR},
      </if>
      <if test="remark != null">
        #{remark,jdbcType=VARCHAR},
      </if>
      <if test="status != null">
        #{status,jdbcType=INTEGER},
      </if>
      <if test="payStatus != null">
        #{payStatus,jdbcType=INTEGER},
      </if>
      <if test="sendNo != null">
        #{sendNo,jdbcType=VARCHAR},
      </if>
      <if test="sendCompany != null">
        #{sendCompany,jdbcType=VARCHAR},
      </if>
      <if test="sendTime != null">
        #{sendTime,jdbcType=TIMESTAMP},
      </if>
      <if test="totalAmount != null">
        #{totalAmount,jdbcType=DECIMAL},
      </if>
      <if test="payAmount != null">
        #{payAmount,jdbcType=DECIMAL},
      </if>
      <if test="noPayAmount != null">
        #{noPayAmount,jdbcType=DECIMAL},
      </if>
      <if test="creatorId != null">
        #{creatorId,jdbcType=BIGINT},
      </if>
      <if test="createdTime != null">
        #{createdTime,jdbcType=TIMESTAMP},
      </if>
      <if test="modifierId != null">
        #{modifierId,jdbcType=BIGINT},
      </if>
      <if test="modifiedTime != null">
        #{modifiedTime,jdbcType=TIMESTAMP},
      </if>
      <if test="dr != null">
        #{dr,jdbcType=INTEGER},
      </if>
    </trim>
  </insert>
  <update id="updateByPrimaryKeySelective" parameterType="com.daigou.model.Order">
    update tb_order
    <set>
      <if test="salerId != null">
        saler_id = #{salerId,jdbcType=BIGINT},
      </if>
      <if test="shopId != null">
        shop_id = #{shopId,jdbcType=BIGINT},
      </if>
      <if test="shopName != null">
        shop_name = #{shopName,jdbcType=VARCHAR},
      </if>
      <if test="productId != null">
        product_id = #{productId,jdbcType=BIGINT},
      </if>
      <if test="productName != null">
        product_name = #{productName,jdbcType=VARCHAR},
      </if>
      <if test="price != null">
        price = #{price,jdbcType=DECIMAL},
      </if>
      <if test="num != null">
        num = #{num,jdbcType=DECIMAL},
      </if>
      <if test="cate != null">
        cate = #{cate,jdbcType=VARCHAR},
      </if>
      <if test="spec != null">
        spec = #{spec,jdbcType=VARCHAR},
      </if>
      <if test="brand != null">
        brand = #{brand,jdbcType=VARCHAR},
      </if>
      <if test="productionPlace != null">
        production_place = #{productionPlace,jdbcType=VARCHAR},
      </if>
      <if test="buyerCode != null">
        buyer_code = #{buyerCode,jdbcType=VARCHAR},
      </if>
      <if test="buyerName != null">
        buyer_name = #{buyerName,jdbcType=VARCHAR},
      </if>
      <if test="address != null">
        address = #{address,jdbcType=VARCHAR},
      </if>
      <if test="picMain != null">
        pic_main = #{picMain,jdbcType=VARCHAR},
      </if>
      <if test="remark != null">
        remark = #{remark,jdbcType=VARCHAR},
      </if>
      <if test="status != null">
        status = #{status,jdbcType=INTEGER},
      </if>
      <if test="payStatus != null">
        pay_status = #{payStatus,jdbcType=INTEGER},
      </if>
      <if test="sendNo != null">
        send_no = #{sendNo,jdbcType=VARCHAR},
      </if>
      <if test="sendCompany != null">
        send_company = #{sendCompany,jdbcType=VARCHAR},
      </if>
      <if test="sendTime != null">
        send_time = #{sendTime,jdbcType=TIMESTAMP},
      </if>
      <if test="totalAmount != null">
        total_amount = #{totalAmount,jdbcType=DECIMAL},
      </if>
      <if test="payAmount != null">
        pay_amount = #{payAmount,jdbcType=DECIMAL},
      </if>
      <if test="noPayAmount != null">
        no_pay_amount = #{noPayAmount,jdbcType=DECIMAL},
      </if>
      <if test="creatorId != null">
        creator_id = #{creatorId,jdbcType=BIGINT},
      </if>
      <if test="createdTime != null">
        created_time = #{createdTime,jdbcType=TIMESTAMP},
      </if>
      <if test="modifierId != null">
        modifier_id = #{modifierId,jdbcType=BIGINT},
      </if>
      <if test="modifiedTime != null">
        modified_time = #{modifiedTime,jdbcType=TIMESTAMP},
      </if>
      <if test="dr != null">
        dr = #{dr,jdbcType=INTEGER},
      </if>
    </set>
    where id = #{id,jdbcType=BIGINT}
  </update>
  <update id="updateByPrimaryKey" parameterType="com.daigou.model.Order">
    update tb_order
    set saler_id = #{salerId,jdbcType=BIGINT},
      shop_id = #{shopId,jdbcType=BIGINT},
      shop_name = #{shopName,jdbcType=VARCHAR},
      product_id = #{productId,jdbcType=BIGINT},
      product_name = #{productName,jdbcType=VARCHAR},
      price = #{price,jdbcType=DECIMAL},
      num = #{num,jdbcType=DECIMAL},
      cate = #{cate,jdbcType=VARCHAR},
      spec = #{spec,jdbcType=VARCHAR},
      brand = #{brand,jdbcType=VARCHAR},
      production_place = #{productionPlace,jdbcType=VARCHAR},
      buyer_code = #{buyerCode,jdbcType=VARCHAR},
      buyer_name = #{buyerName,jdbcType=VARCHAR},
      address = #{address,jdbcType=VARCHAR},
      pic_main = #{picMain,jdbcType=VARCHAR},
      remark = #{remark,jdbcType=VARCHAR},
      status = #{status,jdbcType=INTEGER},
      pay_status = #{payStatus,jdbcType=INTEGER},
      send_no = #{sendNo,jdbcType=VARCHAR},
      send_company = #{sendCompany,jdbcType=VARCHAR},
      send_time = #{sendTime,jdbcType=TIMESTAMP},
      total_amount = #{totalAmount,jdbcType=DECIMAL},
      pay_amount = #{payAmount,jdbcType=DECIMAL},
      no_pay_amount = #{noPayAmount,jdbcType=DECIMAL},
      creator_id = #{creatorId,jdbcType=BIGINT},
      created_time = #{createdTime,jdbcType=TIMESTAMP},
      modifier_id = #{modifierId,jdbcType=BIGINT},
      modified_time = #{modifiedTime,jdbcType=TIMESTAMP},
      dr = #{dr,jdbcType=INTEGER}
    where id = #{id,jdbcType=BIGINT}
  </update>
  <update id="updatePayAmountByPrimaryKey" parameterType="com.daigou.model.Order">
    update tb_order
    set
      pay_status = #{payStatus,jdbcType=INTEGER},
      pay_amount = pay_amount + #{payAmount,jdbcType=DECIMAL},
      no_pay_amount = no_pay_amount - #{payAmount,jdbcType=DECIMAL},
      modifier_id = #{modifierId,jdbcType=BIGINT},
      modified_time = #{modifiedTime,jdbcType=TIMESTAMP}
    where id = #{id,jdbcType=BIGINT}
  </update>
</mapper>