package com.daigou.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.daigou.dao.OrderMapper;
import com.daigou.dto.ServiceResult;
import com.daigou.model.Order;
import com.daigou.model.Product;
import com.daigou.model.User;
import com.daigou.service.OrderService;
import com.daigou.service.ProductService;
import com.daigou.service.UserService;
import com.daigou.tools.GenerateUUID;

/**  
 * @Title: OrderServiceImpl.java
 * @Package com.daigou.service
 * @Description: TODO
 * @author lizhi
 * @date 2018年3月27日
 */
@Service("orderService")  
public class OrderServiceImpl implements OrderService {
	@Autowired  
	protected OrderMapper orderMapper;
	
	@Autowired  
	protected UserService userService;
	
	@Autowired  
	protected ProductService productService;
	
	public ServiceResult insertOrUpdate(Order order){
		if(order.getTotalAmount() == null){
			order.setTotalAmount(order.getPrice());
		}
		if(order.getId() == null){
			if(order.getProductId() == null || "".equals(order.getProductId())){
				Product product = new Product();
				product.setName(order.getProductName());
				product.setSalePrice(order.getPrice());
				product.setMarketPrice(order.getPrice());
				product.setCate(order.getCate());
				product.setBrand(order.getBrand());
				product.setProductionPlace(order.getProductionPlace());
				product.setSpec(order.getSpec());
				product.setPicMain(order.getPicMain());
				product.setSalerId(order.getSalerId());
				product.setShopId(order.getShopId());
				product.setCreatedTime(new Date());
				product.setCreatorId(order.getCreatorId());
				//1上架
				product.setStatus(1);
				//0，草稿
				product.setDr(0);
				ServiceResult result = productService.insertOrUpdate(product);
				order.setProductId(Long.parseLong(result.getData().toString()));
			}
			order.setId(GenerateUUID.generateUUID());
			order.setNoPayAmount(order.getTotalAmount());
			orderMapper.insertSelective(order);
		}else{
			order.setModifiedTime(new Date());
			order.setModifierId(order.getCreatorId());
			//修改时，不要改创建时间
			order.setCreatedTime(null);
			order.setCreatorId(null);
			orderMapper.updateByPrimaryKeySelective(order);
		}
		User userInfo = userService.selectByLoingName(order.getBuyerCode());
		if(userInfo == null){
			userInfo = new User();
			userInfo.setLoginName(order.getBuyerCode());
			userInfo.setMobile(order.getBuyerCode());
			userInfo.setName(order.getBuyerName());
			userInfo.setAddress(order.getAddress());
			userInfo.setId(GenerateUUID.generateUUID());
			userInfo.setType(2);//2代表买家
			userInfo.setCreatorId(order.getCreatorId());
			userInfo.setCreatedTime(new Date());
			userService.register(userInfo);
		}else{
			userInfo.setLoginName(order.getBuyerCode());
			userInfo.setMobile(order.getBuyerCode());
			userInfo.setName(order.getBuyerName());
			userInfo.setAddress(order.getAddress());
			userService.updateByPrimaryKeySelective(userInfo);
		}
		return new ServiceResult(true,"订单保存成功!",order.getId());
	}
	
	public int deleteByPrimaryKey(Long id){
		return orderMapper.deleteByPrimaryKey(id);
	}

	public int insert(Order record){
		return orderMapper.insert(record);
	}

	public int insertSelective(Order record){
		return orderMapper.insertSelective(record);
	}
	
	public List<Order> selectByShopId(Long shopId,String searchKey){
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("shopId", shopId);
		paramMap.put("searchKey", searchKey);
		List<Order> orderList = orderMapper.selectByShopId(paramMap);
		for(Order order:orderList){
			order.setStatusName(order.getStatus());
		}
		return orderList;
	}
	/**
	 * 查询草稿状态的order
	 * @param shopId
	 * @param searchKey
	 * @return
	 */
	public List<Order> selectCaogaoByShopId(Long shopId,String searchKey){
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("shopId", shopId);
		paramMap.put("searchKey", searchKey);
		List<Order> orderList =  orderMapper.selectCaogaoByShopId(paramMap);
		for(Order order:orderList){
			order.setStatusName(order.getStatus());
		}
		return orderList;
	}
	
	public Integer selectTotalCountByShopId(Long shopId){
		return orderMapper.selectTotalCountByShopId(shopId);
	}
	
	public Order selectByPrimaryKey(Long id){
		Order order = orderMapper.selectByPrimaryKey(id);
		order.setStatusName(order.getStatus());
		return order;
	}

	public int updateByPrimaryKeySelective(Order record){
		return orderMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(Order record){
		return orderMapper.updateByPrimaryKey(record);
	}
	/**
	 * 更新订单那支付金额
	 * @param orderId
	 * @param payAmount
	 * @return
	 */
	public int updatePayAmountByPrimaryKey(Order record){
		return orderMapper.updatePayAmountByPrimaryKey(record);
	}
}  