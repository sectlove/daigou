package com.daigou.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.daigou.enums.OrderStatusEnum;
import com.daigou.tools.AuthSqlBuilder;

public class Order implements Serializable {
    private Long id;

    private Long shopId;

    private String shopName;

    private Long productId;
    
    private String productName;

    private BigDecimal price = BigDecimal.ZERO;

    private BigDecimal num = BigDecimal.ONE;

    private String cate;

    private String spec;

    private String brand;
    
    private String productionPlace;

    private String buyerCode;
    
    private String buyerWeixin;

    private String buyerName;

    private String address;
    
    private String email;

    private String picMain;

    private String remark;

    private Integer status;
    
    private String statusName;
    //0:未支付，1，支付中，2，已支付
    private Integer payStatus;
    
    private String payStatusName;

    private String sendNo;

    private String sendCompany;

    private Date sendTime;
    
    private BigDecimal orderAmount = BigDecimal.ZERO;
    
    private BigDecimal totalAmount = BigDecimal.ZERO;

    private BigDecimal payAmount = BigDecimal.ZERO;

    private BigDecimal noPayAmount;

    private Long creatorId = AuthSqlBuilder.ALL_AUTH_TAG;

    private Date createdTime;

    private Long modifierId;

    private Date modifiedTime;
    //-1：删除，0，草稿，1，正常
    private Integer dr=1;

    private Long ts = 0L;
    
    private Long newTs = 0L;
    
    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName == null ? null : shopName.trim();
    }
    
    public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName == null ? null : productName.trim();
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getNum() {
        return num;
    }

    public void setNum(BigDecimal num) {
        this.num = num;
    }

    public String getCate() {
        return cate;
    }

    public void setCate(String cate) {
        this.cate = cate;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec == null ? null : spec.trim();
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand == null ? null : brand.trim();
    }

	public String getProductionPlace() {
		return productionPlace;
	}

	public void setProductionPlace(String productionPlace) {
		this.productionPlace = productionPlace;
	}

	public String getBuyerCode() {
        return buyerCode;
    }

    public void setBuyerCode(String buyerCode) {
        this.buyerCode = buyerCode == null ? null : buyerCode.trim();
    }

	public String getBuyerWeixin() {
		return buyerWeixin;
	}

	public void setBuyerWeixin(String buyerWeixin) {
		this.buyerWeixin = buyerWeixin;
	}

	public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName == null ? null : buyerName.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPicMain() {
        return picMain;
    }

    public void setPicMain(String picMain) {
        this.picMain = picMain == null ? null : picMain.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPayStatusName() {
		return payStatusName;
	}

	public void setPayStatusName(Integer payStatus) {
		if(payStatus == 1){
			this.payStatusName = "已支付,待确认";
		}else if(payStatus == 2){
			this.payStatusName = "支付完毕";
		}else{
			this.payStatusName = "支付中";
		}
	}

	public String getStatusName() {
		return statusName;
	}

    public Integer getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(Integer payStatus) {
		this.payStatus = payStatus;
	}

	public void setStatusName(Integer status) {
    	this.statusName = OrderStatusEnum.valueOf(status).getText();
	}
    public String getSendNo() {
        return sendNo;
    }

    public void setSendNo(String sendNo) {
        this.sendNo = sendNo == null ? null : sendNo.trim();
    }

    public String getSendCompany() {
        return sendCompany;
    }

    public void setSendCompany(String sendCompany) {
        this.sendCompany = sendCompany == null ? null : sendCompany.trim();
    }

    public Date getSendTime() {
        return sendTime;
    }

    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

	public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    public BigDecimal getNoPayAmount() {
        return noPayAmount;
    }

    public void setNoPayAmount(BigDecimal noPayAmount) {
        this.noPayAmount = noPayAmount;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Long getModifierId() {
        return modifierId;
    }

    public void setModifierId(Long modifierId) {
        this.modifierId = modifierId;
    }

    public Date getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public Integer getDr() {
        return dr;
    }

    public void setDr(Integer dr) {
        this.dr = dr;
    }

	public Long getTs() {
		return ts;
	}

	public void setTs(Long ts) {
		this.ts = ts;
	}

	public Long getNewTs() {
		return newTs;
	}

	public void setNewTs(Long newTs) {
		this.newTs = newTs;
	}

	public BigDecimal getOrderAmount() {
		return orderAmount;
	}

	public void setOrderAmount(BigDecimal orderAmount) {
		this.orderAmount = orderAmount;
	}
}