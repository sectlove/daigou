package com.daigou.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.daigou.dao.OrderMapper;
import com.daigou.dto.ServiceResult;
import com.daigou.model.Order;
import com.daigou.model.User;
import com.daigou.service.OrderService;
import com.daigou.service.UserService;
import com.daigou.tools.GenerateUUID;

/**  
 * @Title: OrderServiceImpl.java
 * @Package com.daigou.service
 * @Description: TODO
 * @author lizhi
 * @date 2018年3月27日
 */
@Service("orderService")  
public class OrderServiceImpl implements OrderService {
	@Autowired  
	protected OrderMapper orderMapper;
	
	@Autowired  
	protected UserService userService;
	
	public ServiceResult insertOrUpdate(Order order){
		if(order.getTotalAmount() == null){
			order.setTotalAmount(order.getPrice());
		}
		if(order.getId() == null){
			order.setId(GenerateUUID.generateUUID());
			orderMapper.insertSelective(order);
		}else{
			order.setModifiedTime(new Date());
			order.setModifierId(order.getCreatorId());
			//修改时，不要改创建时间
			order.setCreatedTime(null);
			order.setCreatorId(null);
			orderMapper.updateByPrimaryKeySelective(order);
		}
		User userInfo = userService.selectByLoingName(order.getBuyerCode());
		if(userInfo == null){
			userInfo = new User();
			userInfo.setLoginName(order.getBuyerCode());
			userInfo.setMobile(order.getBuyerCode());
			userInfo.setName(order.getBuyerName());
			userInfo.setAddress(order.getAddress());
			userInfo.setId(GenerateUUID.generateUUID());
			userInfo.setType(2);//2代表买家
			userInfo.setCreatorId(order.getCreatorId());
			userInfo.setCreatedTime(new Date());
			userService.register(userInfo);
		}else{
			userInfo.setLoginName(order.getBuyerCode());
			userInfo.setMobile(order.getBuyerCode());
			userInfo.setName(order.getBuyerName());
			userInfo.setAddress(order.getAddress());
			userService.updateByPrimaryKeySelective(userInfo);
		}
		return new ServiceResult(true,"订单保存成功!",order.getId());
	}
	
	public int deleteByPrimaryKey(Long id){
		return orderMapper.deleteByPrimaryKey(id);
	}

	public int insert(Order record){
		return orderMapper.insert(record);
	}

	public int insertSelective(Order record){
		return orderMapper.insertSelective(record);
	}
	
	public List<Order> selectByShopId(Long shopId,String searchKey){
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("shopId", shopId);
		paramMap.put("searchKey", searchKey);
		return orderMapper.selectByShopId(paramMap);
	}
	/**
	 * 查询草稿状态的order
	 * @param shopId
	 * @param searchKey
	 * @return
	 */
	public List<Order> selectCaogaoByShopId(Long shopId,String searchKey){
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("shopId", shopId);
		paramMap.put("searchKey", searchKey);
		return orderMapper.selectCaogaoByShopId(paramMap);
	}
	
	public Integer selectTotalCountByShopId(Long shopId){
		return orderMapper.selectTotalCountByShopId(shopId);
	}
	
	public Order selectByPrimaryKey(Long id){
		return orderMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(Order record){
		return orderMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(Order record){
		return orderMapper.updateByPrimaryKey(record);
	}
}  