<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path;
%>
<!-- 1234567890begin -->
<div class="tab-pane " id="tab_3">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption"><i class="icon-reorder"></i>基本信息</div>
			<div class="tools">
				<a href="javascript:;" class="collapse"></a>
				<a href="#portlet-config" data-toggle="modal" class="config"></a>
				<a href="javascript:;" class="reload"></a>
				<a href="javascript:;" class="remove"></a>
			</div>
		</div>
		<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<div class="form-horizontal form-view">
				<h3 class="form-section">个人信息</h3>
				<div class="row-fluid">
					<div class="span6 ">
						<div class="control-group">
							<label class="control-label" for="firstName">姓名:</label>
							<div class="controls">
								<span class="text">张三</span>
							</div>
						</div>
					</div>
					<!--/span-->
					<div class="span6 ">
						<div class="control-group">
							<label class="control-label" for="lastName">性别:</label>
							<div class="controls">
								<span class="text">男</span>
							</div>
						</div>
					</div>
					<!--/span-->
				</div>
				<!--/row-->
				<div class="row-fluid">
					<div class="span6 ">
						<div class="control-group">
							<label class="control-label" >出生年月:</label>
							<div class="controls">
								<span class="text">1988年9月</span> 
							</div>
						</div>
					</div>
					<!--/span-->
					<div class="span6 ">
						<div class="control-group">
							<label class="control-label" >籍贯</label>
							<div class="controls">
								<span class="text bold">河北省保定市</span>
							</div>
						</div>
					</div>
					<!--/span-->
				</div>
				<!--/row-->        
				<div class="row-fluid">
					<div class="span6 ">
						<div class="control-group">
							<label class="control-label" >毕业院校:</label>
							<div class="controls">
								<span class="text bold">北京大学</span>
							</div>
						</div>
					</div>
					<!--/span-->
					<div class="span6 ">
						<div class="control-group">
							<label class="control-label" >毕业时间:</label>
							<div class="controls">                                                
								<span class="text bold">2011年7月1日</span>
							</div>
						</div>
					</div>
					<!--/span-->
				</div>
				<!--/row-->
				<div class="row-fluid">
					<div class="span6 ">
						<div class="control-group">
							<label class="control-label" >学历:</label>
							<div class="controls">
								<span class="text bold">本科</span>
							</div>
						</div>
					</div>
					<!--/span-->
					<div class="span6 ">
						<div class="control-group">
							<label class="control-label" >专业:</label>
							<div class="controls">                                                
								<span class="text bold">信息管理与信息系统</span>
							</div>
						</div>
					</div>
					<!--/span-->
				</div>
				<!--/row-->
				<!--/row-->
				<div class="row-fluid">
					<div class="span6 ">
						<div class="control-group">
							<label class="control-label" >工作年限:</label>
							<div class="controls">
								<span class="text bold">5年</span>
							</div>
						</div>
					</div>
					<!--/span-->
					<div class="span6 ">
						<div class="control-group">
							<label class="control-label" >政治面貌:</label>
							<div class="controls">                                                
								<span class="text bold">党员</span>
							</div>
						</div>
					</div>
					<!--/span-->
				</div>
				<!--/row-->
				<h3 class="form-section">求职意向</h3>
				<div class="row-fluid">
					<div class="span12 ">
						<div class="control-group">
							<label class="control-label" >应聘职位：</label>
							<div class="controls">
								<span class="text">java开发经理</span>
							</div>
						</div>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span6 ">
						<div class="control-group">
							<label class="control-label" >工作地区</label>
							<div class="controls">
								<span class="text">北京</span>
							</div>
						</div>
					</div>
					<!--/span-->
					<div class="span6">
						<div class="control-group">
							<label class="control-label" >可到职时间：</label>
							<div class="controls">
								<span class="text">2周内</span>
							</div>
						</div>
					</div>
					<!--/span-->
				</div>
				<!--/row-->           
				<div class="row-fluid">
					<div class="span6 ">
						<div class="control-group">
							<label class="control-label" >求职类型:</label>
							<div class="controls">
								<span class="text">全职</span>
							</div>
						</div>
					</div>
					<!--/span-->
					<div class="span6 ">
						<div class="control-group">
							<label class="control-label" >希望就职的其他岗位:</label>
							<div class="controls">
								<span class="text">java高级开发工程师</span>
							</div>
						</div>
					</div>
					<!--/span-->
				</div>
				<h3 class="form-section">专业技能</h3>
					<div class="row-fluid">
						<div class="span12 ">
							<div class="control-group">
								<label class="control-label" >1</label>
								<div class="controls">
									<span class="text">熟练使用java语言，java基础牢固</span>
								</div>
							</div>
						</div>
						<div class="span12 ">
							<div class="control-group">
								<label class="control-label" >2</label>
								<div class="controls">
									<span class="text">熟练使用开源框架，如spring，hibernate，struts，jquery，extjs等</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-actions">
					<button type="submit" class="btn blue"><i class="icon-pencil"></i>编辑</button>
				</div>
			</div>
			<!-- END FORM-->  
		</div>
	</div>
</div>
<!-- BEGIN PAGE CONTENT-->          
<div class="container-fluid">
	<!-- BEGIN SAMPLE TABLE PORTLET-->
	<div class="portlet box red">
		<div class="portlet-title">
			<div class="caption"><i class="icon-cogs"></i>受教育培训经历</div>
			<div class="tools">
				<a href="javascript:;" class="collapse"></a>
				<a href="#portlet-config" data-toggle="modal" class="config"></a>
				<a href="javascript:;" class="reload"></a>
				<a href="javascript:;" class="remove"></a>
			</div>
		</div>
		<div class="portlet-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>起始年月</th>
						<th>结束年月</th>
						<th class="hidden-480">教育机构</th>
						<th>获得证书</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>2007年9月</td>
						<td>2011年7月</td>
						<td class="hidden-480">北京大学</td>
						<td>毕业证、学位证</td>
					</tr>
					<tr>
						<td>2</td>
						<td>2004</td>
						<td>2007</td>
						<td class="hidden-480">北京四中</td>
						<td>毕业证</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td class="hidden-480"></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<!-- END SAMPLE TABLE PORTLET-->
	<!-- BEGIN CONDENSED TABLE PORTLET-->
	<div class="portlet box green">
		<div class="portlet-title">
			<div class="caption"><i class="icon-picture"></i>工作经历</div>
			<div class="tools">
				<a href="javascript:;" class="collapse"></a>
				<a href="#portlet-config" data-toggle="modal" class="config"></a>
				<a href="javascript:;" class="reload"></a>
				<a href="javascript:;" class="remove"></a>
			</div>
		</div>
		<div class="portlet-body">
			<table class="table table-condensed table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>起始年月</th>
						<th>结束年月</th>
						<th class="hidden-480">公司名称</th>
						<th>职务</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>2014年3月</td>
						<td>2016年10月</td>
						<td class="hidden-480">鼎捷软件</td>
						<td>开发经理</td>
						<td><span class="label label-danger">编辑</span></td>
					</tr>
					<tr>
						<td>2</td>
						<td>2012年3月</td>
						<td>2014年3月</td>
						<td class="hidden-480">用友软件</td>
						<td>java高级开发工程师</td>
						<td><span class="label label-danger">编辑</span></td>
					</tr>
					<tr>
						<td>3</td>
						<td>2011年5月</td>
						<td>2012年5月</td>
						<td class="hidden-480">中科软科技股份有限公司</td>
						<td>java开发</td>
						<td><span class="label label-danger">编辑</span></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<!-- END CONDENSED TABLE PORTLET-->
	<!-- BEGIN SAMPLE TABLE PORTLET-->
	<div class="portlet box purple">
		<div class="portlet-title">
			<div class="caption"><i class="icon-comments"></i>项目经验</div>
			<div class="tools">
				<a href="javascript:;" class="collapse"></a>
				<a href="#portlet-config" data-toggle="modal" class="config"></a>
				<a href="javascript:;" class="reload"></a>
				<a href="javascript:;" class="remove"></a>
			</div>
		</div>
		<div class="portlet-body">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>起始年月</th>
						<th>结束年月</th>
						<th class="hidden-480">项目名称</th>
						<th>职务</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>2011年4月</td>
						<td>2012年5月</td>
						<td class="hidden-480">京东商城</td>
						<td>java工程师</td>
						<td><span class="label label-success">编辑</span></td>
					</tr>
					<tr>
						<td>2</td>
						<td>2011年4月</td>
						<td>2012年5月</td>
						<td class="hidden-480">阿狸商城</td>
						<td>java工程师</td>
						<td><span class="label label-success">编辑</span></td>
					</tr>
					<tr>
						<td>3</td>
						<td>2011年4月</td>
						<td>2012年5月</td>
						<td class="hidden-480">天猫商城</td>
						<td>java工程师</td>
						<td><span class="label label-success">编辑</span></td>
					</tr>
					<tr>
						<td>4</td>
						<td>2011年4月</td>
						<td>2012年5月</td>
						<td class="hidden-480">智互联商城</td>
						<td>java工程师</td>
						<td><span class="label label-success">编辑</span></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<!-- END SAMPLE TABLE PORTLET-->
</div>
<!-- 1234567890end -->