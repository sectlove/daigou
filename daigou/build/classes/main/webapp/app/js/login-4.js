var Login = function (baselocation) {

	var handleLogin = function(baselocation) {
		$('.login-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	            	loginName: {
	                    required: true
	                },
	                pwd: {
	                    required: true
	                },
	                verifyCode: {
	                    required: false
	                },
	                remember: {
	                    required: false
	                }
	            },

	            messages: {
	            	loginName: {
	                    required: "用户名必填！"
	                },
	                pwd: {
	                    required: "密码必填！"
	                },
	                verifyCode: {
	                    required: "验证码必填！"
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
	                $('.alert-danger', $('.login-form')).show();
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.insertAfter(element.closest('.input-icon'));
	            },

	           submitHandler: function (form) {
	            	var param = $('#login-form').serialize();
	        		$.ajax({
						url : "login",
						type : "post",
						dataType : "json",
						data : param,
						success : function(result) {
							if (result.success) {
								location.href = 'index';
							} else {
								$("#login_errmsg").text(result.message);
								$('.alert-danger', $('.login-form')).show();
								if(result.data){
									if(result.data.showVerify!=''){
										$("#showVerifyDiv").show();
									}
								}
							}
						}
					});
            }
	        });

	        $('.login-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.login-form').validate().form()) {
//	                    $('.login-form').submit();
	                }
	                return false;
	            }
	        });
	}

	var handleForgetPassword = function (baselocation) {
		$('.forget-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	            	findPwdEmail: {
	                    required: true,
	                    email: true
	                }
	            },

	            messages: {
	            	findPwdEmail: {
	                    required: "请输入邮箱！",
	                    email:"请输入有效的邮箱地址！"
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
	            	$('.alert-danger', $('.forget-form')).show();
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.insertAfter(element.closest('.input-icon'));
	            },

	            submitHandler: function (form) {
	            	var param = $('#forget-form').serialize();
            		$.ajax({
						url :"forgetPwd",
						type : "post",
						dataType : "json",
						data : param,
						success : function(result) {
							if (result.success) {
								$("#infomsg").text(result.message);
								$("#btnerr").click();
								$("#forget_errmsg").text();
								$('.alert-danger', $('.forget-form')).hide();
							} else {
								$("#forget_errmsg").text(result.message);
								$('.alert-danger', $('.forget-form')).show();
							}
						}
					});
	            }
	        });

	        $('.forget-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.forget-form').validate().form()) {
	                    $('.forget-form').submit();
	                }
	                return false;
	            }
	        });

	        jQuery('#forget-password').click(function () {
	            jQuery('.login-form').hide();
	            jQuery('.forget-form').show();
	        });

	        jQuery('#back-btn').click(function () {
	            jQuery('.login-form').show();
	            jQuery('.forget-form').hide();
	        });

	}

	var handleRegister = function (baselocation) {

		        function format(state) {
            if (!state.id) { return state.text; }
            var $state = $(
             '<span><img src="assets/global/img/flags/' + state.element.value.toLowerCase() + '.png" class="img-flag" /> ' + state.text + '</span>'
            );
            
            return $state;
        }

        if (jQuery().select2 && $('#country_list').size() > 0) {
            $("#country_list").select2({
	            placeholder: '<i class="fa fa-map-marker"></i>&nbsp;Select a Country',
	            templateResult: format,
                templateSelection: format,
                width: 'auto', 
	            escapeMarkup: function(m) {
	                return m;
	            }
	        });


	        $('#country_list').change(function() {
	            $('.register-form').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
	        });
    	}


         $('.register-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	            	loginName: {
	                    required: true
	                },
	                pwd: {
	                    required: true
	                },
	                rpassword: {
	                    equalTo: "#register_password"
	                },
	                email: {
	                    required: true,
	                    email: true
	                },
	                tnc: {
	                    required: true
	                }
	            },

	            messages: { // custom messages for radio buttons and checkboxes
	            	loginName: {
	                    required: "用户名必填！"
	                },
	                pwd: {
	                    required: "密码必填！"
	                },
	                rpassword: {
	                    required: "重复密码必填！",
	                    equalTo:"请输入相同的重复密码！"
	                },
	                email: {
	                	email: "邮箱必填！",
	                	email:"请输入有效的邮箱地址！"
	                },
	                tnc: {
	                    required: "请先接受注册协议."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                if (element.attr("name") == "tnc") { // insert checkbox errors after the container                  
	                    error.insertAfter($('#register_tnc_error'));
	                } else if (element.closest('.input-icon').size() === 1) {
	                    error.insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.insertAfter(element);
	                }
	            },

	            submitHandler: function (form) {
	            	var param = $('#register-form').serialize();
            		$.ajax({
						url :"register",
						type : "post",
						dataType : "json",
						data : param,
						success : function(result) {
							if (result.success) {
								$("#infomsg").text(result.message);
								$("#btnerr").click();
								$("#forget_errmsg").text();
								$('.alert-danger', $('.register-form')).hide();
								location.href = 'index';
							} else {
								$("#register_errmsg").text(result.message);
								$('.alert-danger', $('.register-form')).show();
							}
						}
					});
	                //window.location.href = "register";
	            }
	        });

			$('.register-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.register-form').validate().form()) {
	                    $('.register-form').submit();
	                }
	                return false;
	            }
	        });

	        jQuery('#register-btn').click(function () {
	            jQuery('.login-form').hide();
	            jQuery('.register-form').show();
	        });

	        jQuery('#register-back-btn').click(function () {
	            jQuery('.login-form').show();
	            jQuery('.register-form').hide();
	        });
	}
    
    return {
        //main function to initiate the module
        init: function () {
        	
            handleLogin();
            handleForgetPassword();
            handleRegister();    

            // init background slide images
		    $.backstretch([
		        "assets/pages/media/bg/1.jpg",
		        "assets/pages/media/bg/2.jpg",
		        "assets/pages/media/bg/3.jpg",
		        "assets/pages/media/bg/4.jpg"
		        ], {
		          fade: 1000,
		          duration: 8000
		    	}
        	);
        }
    };

}();

jQuery(document).ready(function() {
    Login.init();
});